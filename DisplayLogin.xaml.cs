﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.Caching;
using ADG_Assistance.DB;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Reflection;
using ADG_Assistance.Functions;

namespace ADG_Assistance.Vue.DataDisplay
{
    /// <summary>
    /// Logique d'interaction pour DispayLogin.xaml
    /// </summary>
    public partial class DispayLogin : Window
    {
        private ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        ObjectCache cache = MemoryCache.Default;
        bool close = false;
        bool firstConnexion = true;
        public DispayLogin()
        {
            InitializeComponent();
            CBB_ListeLogin.ItemsSource = (from membres in dbEntity.Membres select membres.user).ToList();
            BTN_Connect_Inscrire.Visibility = Visibility.Hidden;
        }

        private void BTN_Connect_Click(object sender, RoutedEventArgs e)
        {
            connect();
        }

        private void connect()
        {

            if(TXT_Password.Password != "")
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    string source = "";
                    var password_bdd = from membres in dbEntity.Membres where membres.user == CBB_ListeLogin.SelectedItem.ToString() select membres.password;

                    foreach (var hashPassword in password_bdd)
                    {
                        source = hashPassword;
                    }

                    if (VerifyMd5Hash(md5Hash, TXT_Password.Password, source))
                    {

                        DateTimeOffset date = DateTimeOffset.MaxValue;
                        cache.Add("Username", CBB_ListeLogin.SelectedItem.ToString(), date);
                        var role = (from membres in dbEntity.Membres where membres.user == CBB_ListeLogin.SelectedItem.ToString() select membres.role);
                        foreach (var donneRole in role)
                        {
                            cache.Add("Role", donneRole, date);
                        }

                        var mail = (from membres in dbEntity.Membres where membres.user == CBB_ListeLogin.SelectedItem.ToString() select membres.mail);
                        foreach (var donneeMail in mail)
                        {
                            cache.Add("Mail", donneeMail, date);
                        }

                        var id = (from membres in dbEntity.Membres where membres.user == CBB_ListeLogin.SelectedItem.ToString() select membres.id);
                        foreach (var donnneId in id)
                        {
                            cache.Add("Id", donnneId, date);
                        }

                        MainWindow home = new MainWindow(true);
                        home.Show();
                        showNotification();
                        this.Hide();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Erreur de connexion, mauvais mot de passe.");
                    }

                }
            }

            
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

           
            StringBuilder sBuilder = new StringBuilder();

            
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

           
            return sBuilder.ToString();
        }

        
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
           
            string hashOfInput = GetMd5Hash(md5Hash, input);

            
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!close)
            {
                //System.Environment.Exit(1);
            }
            
        }

        private void CBB_ListeLogin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int index = CBB_ListeLogin.SelectedIndex;
            string username = CBB_ListeLogin.Items[index].ToString();


            var reqConnexion = from membres in dbEntity.Membres
                               where membres.user == username
                               select membres.FirstConnexion;
           
            

            foreach(var result in reqConnexion)
            {
                firstConnexion = (bool) result;
            }

            if (!firstConnexion)
            {
                LBL_Mot_de_passe.Content = "Nouveau mot de passe";
                System.Windows.MessageBox.Show("Première connexion, saisir votre nouveau mot de passe.");
                BTN_Connect.Visibility = Visibility.Hidden;
                BTN_Connect_Inscrire.Visibility = Visibility.Visible;
                firstConnexion = true;
            }
            else
            {
                LBL_Mot_de_passe.Content = "Mot de passe";
                BTN_Connect.Visibility = Visibility.Visible;
                BTN_Connect_Inscrire.Visibility = Visibility.Hidden;
                firstConnexion = true;
            }
        }

        private void BTN_Connect_Inscrire_Click(object sender, RoutedEventArgs e)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, TXT_Password.Password);

                var connectionString = Function.connectDatabase();
                SqlConnection con = new SqlConnection(connectionString);
                try
                {
                    con.Open();
                    SqlCommand reqUpdatePass = new SqlCommand();
                    reqUpdatePass.Connection = con;
                    reqUpdatePass.CommandText = "UPDATE Membres SET Password = @pass WHERE [user] = @id";
                    reqUpdatePass.Parameters.AddWithValue("@id", CBB_ListeLogin.Text);
                    reqUpdatePass.Parameters.AddWithValue("@pass", hash);


                    reqUpdatePass.ExecuteNonQuery();

                    SqlCommand reqUpdateConnexion = new SqlCommand();
                    reqUpdateConnexion.Connection = con;

                    reqUpdateConnexion.CommandText = "UPDATE Membres SET FirstConnexion = @FT WHERE [user] = @id";
                    reqUpdateConnexion.Parameters.AddWithValue("@id", CBB_ListeLogin.Text);
                    reqUpdateConnexion.Parameters.AddWithValue("@FT", 1);

                    reqUpdateConnexion.ExecuteNonQuery();
                }
                catch
                {

                }

                LBL_Mot_de_passe.Content = "Mot de passe";
                BTN_Connect.Visibility = Visibility.Visible;
                BTN_Connect_Inscrire.Visibility = Visibility.Hidden;

            }
        }

        private NotifyIcon _notifyIcon;

        public void showNotification()
        {
            _notifyIcon = new NotifyIcon();
            _notifyIcon.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location);
            _notifyIcon.BalloonTipClosed += (s, e) => _notifyIcon.Visible = false;
            _notifyIcon.Visible = true;
            string idCache = cache.Get("Id").ToString();
            int idMember = Int32.Parse(idCache);
            var reqNbrAppel = from appel in dbEntity.Appel
                              join appelmembre in dbEntity.T_AppelMembres on appel.idAppel equals appelmembre.idAppel
                              where appelmembre.idMembres == idMember
                              where appel.Traitement == false
                              select appel.idAppel;

            int nbrAppel = 0;

            foreach(var result in reqNbrAppel)
            {
                nbrAppel = nbrAppel + 1;
            }

            if (nbrAppel > 0)
            {
                _notifyIcon.ShowBalloonTip(3000, "ADG ASSISTANCE - APPELS", "Il vous reste " + nbrAppel + " appels à traité.", ToolTipIcon.Info);
            }
            
        }

        private void TXT_Password_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                connect();
                e.Handled = true;
            }
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                connect();
                e.Handled = true;
            }
        }
    }
}
