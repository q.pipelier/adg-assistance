﻿#pragma checksum "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1ABA49A282C4339E07CDA9F4FC5E56EF6CEE9DE6"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ADG_Assistance.Vue.DataDisplay {
    
    
    /// <summary>
    /// DisplayBI
    /// </summary>
    public partial class DisplayBI : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 25 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DG_BI;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_AddBI;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_ModifyBI;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_DeleteBI;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_ClearFilters;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_Research;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_NomClient;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_NumContrat;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_NumBI;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_Date;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_NumBI;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_NumContrat;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_NomClient;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CBX_DateEnable;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker Date_Date;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ADG_Assistance;component/vue/datadisplay/displaybi.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            ((ADG_Assistance.Vue.DataDisplay.DisplayBI)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Page_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.DG_BI = ((System.Windows.Controls.DataGrid)(target));
            
            #line 25 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.DG_BI.Sorting += new System.Windows.Controls.DataGridSortingEventHandler(this.DG_BI_Sorting);
            
            #line default
            #line hidden
            return;
            case 4:
            this.BTN_AddBI = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.BTN_AddBI.Click += new System.Windows.RoutedEventHandler(this.BTN_AddBI_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.BTN_ModifyBI = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.BTN_ModifyBI.Click += new System.Windows.RoutedEventHandler(this.BTN_ModifyBI_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.BTN_DeleteBI = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.BTN_DeleteBI.Click += new System.Windows.RoutedEventHandler(this.BTN_DeleteBI_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BTN_ClearFilters = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.BTN_ClearFilters.Click += new System.Windows.RoutedEventHandler(this.BTN_ClearFilters_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.BTN_Research = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.BTN_Research.Click += new System.Windows.RoutedEventHandler(this.BTN_Research_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.LBL_NomClient = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.LBL_NumContrat = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.LBL_NumBI = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.LBL_Date = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.TXT_NumBI = ((System.Windows.Controls.TextBox)(target));
            
            #line 68 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.TXT_NumBI.KeyUp += new System.Windows.Input.KeyEventHandler(this.CheckEnterPressed);
            
            #line default
            #line hidden
            return;
            case 14:
            this.TXT_NumContrat = ((System.Windows.Controls.TextBox)(target));
            
            #line 69 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.TXT_NumContrat.KeyUp += new System.Windows.Input.KeyEventHandler(this.CheckEnterPressed);
            
            #line default
            #line hidden
            return;
            case 15:
            this.TXT_NomClient = ((System.Windows.Controls.TextBox)(target));
            
            #line 70 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.TXT_NomClient.KeyUp += new System.Windows.Input.KeyEventHandler(this.CheckEnterPressed);
            
            #line default
            #line hidden
            return;
            case 16:
            this.CBX_DateEnable = ((System.Windows.Controls.CheckBox)(target));
            
            #line 77 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.CBX_DateEnable.Checked += new System.Windows.RoutedEventHandler(this.CBX_DateEnable_Checked);
            
            #line default
            #line hidden
            
            #line 77 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.CBX_DateEnable.Unchecked += new System.Windows.RoutedEventHandler(this.CBX_DateEnable_Unchecked);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Date_Date = ((System.Windows.Controls.DatePicker)(target));
            
            #line 78 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            this.Date_Date.KeyUp += new System.Windows.Input.KeyEventHandler(this.CheckEnterPressed);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 3:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.Controls.Control.MouseDoubleClickEvent;
            
            #line 37 "..\..\..\..\Vue\DataDisplay\DisplayBI.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.DG_BIRow_DoubleClick);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

