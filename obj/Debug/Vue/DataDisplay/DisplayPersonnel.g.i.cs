﻿#pragma checksum "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "03CB0948EDF7BF5AFEF2FE03FB84F56C36388119"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ADG_Assistance.Vue.DataDisplay {
    
    
    /// <summary>
    /// DisplayPersonnel
    /// </summary>
    public partial class DisplayPersonnel : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 37 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DG_Personnel;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn civiliteColumn;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn prenomColumn;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn nomColumn;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn fonctionColumn;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn telephoneColumn;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn mailColumn;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_AddPersonnel;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_ModifyPersonnel;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_DeletePersonnel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ADG_Assistance;component/vue/datadisplay/displaypersonnel.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            ((ADG_Assistance.Vue.DataDisplay.DisplayPersonnel)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Page_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.DG_Personnel = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 3:
            this.civiliteColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 4:
            this.prenomColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 5:
            this.nomColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 6:
            this.fonctionColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 7:
            this.telephoneColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 8:
            this.mailColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 10:
            this.BTN_AddPersonnel = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            this.BTN_AddPersonnel.Click += new System.Windows.RoutedEventHandler(this.BTN_AddPersonnel_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.BTN_ModifyPersonnel = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            this.BTN_ModifyPersonnel.Click += new System.Windows.RoutedEventHandler(this.BTN_ModifyPersonnel_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BTN_DeletePersonnel = ((System.Windows.Controls.Button)(target));
            
            #line 66 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            this.BTN_DeletePersonnel.Click += new System.Windows.RoutedEventHandler(this.BTN_DeletePersonnel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 9:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.Controls.Control.MouseDoubleClickEvent;
            
            #line 51 "..\..\..\..\Vue\DataDisplay\DisplayPersonnel.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.DG_PersonnelRow_DoubleClick);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

