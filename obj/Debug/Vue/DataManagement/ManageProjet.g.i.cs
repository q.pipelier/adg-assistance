﻿#pragma checksum "..\..\..\..\Vue\DataManagement\ManageProjet.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "BFE6F0FF3ABDD5D4E8712F4C4155A8CC46EB4F39"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using ADG_Assistance.Vue.DataManagement;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ADG_Assistance.Vue.DataManagement {
    
    
    /// <summary>
    /// ManageProjet
    /// </summary>
    public partial class ManageProjet : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox grpDataProject;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_NomClient;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetNom;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_DataGridContactProjet;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_DataGridServeurProjet;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetCommentaire;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CMB_NomClient;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_NomClient;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetNom;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_AddContactProjet;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_RemoveContactProjet;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox GRB_ProjetBddADG;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetADGBddNom;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetADGBddUserNom;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetADGBddPassword;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetADGBddNom;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetADGBddUserNom;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetADGBddPassword;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox GRB_ProjetBddClient;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetClientBddNom;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetClientBddUserNom;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetClientBddPassword;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetClientBddNom;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetClientBddUserNom;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetClientBddPassword;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetClickOnce;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetClickOnce;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LBL_ProjetGit;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetGit;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox CBX_ProjetPresenceBdd;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DG_ContactProjet;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_Redmine;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TXT_ProjetCommentaire;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_AcceptProjet;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_CancelProjet;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DG_Assistance;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_DeleteAssistance;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_ModifyAssistance;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BTN_AddAssistance_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ADG_Assistance;component/vue/datamanagement/manageprojet.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            ((ADG_Assistance.Vue.DataManagement.ManageProjet)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 10 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            ((ADG_Assistance.Vue.DataManagement.ManageProjet)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.Window_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            this.grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.grpDataProject = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 4:
            this.LBL_NomClient = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.LBL_ProjetNom = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.LBL_DataGridContactProjet = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.LBL_DataGridServeurProjet = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.LBL_ProjetCommentaire = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.CMB_NomClient = ((System.Windows.Controls.ComboBox)(target));
            
            #line 34 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.CMB_NomClient.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CMB_NomClient_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.TXT_NomClient = ((System.Windows.Controls.TextBox)(target));
            
            #line 36 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.TXT_NomClient.KeyUp += new System.Windows.Input.KeyEventHandler(this.TXT_NomClient_KeyUp);
            
            #line default
            #line hidden
            return;
            case 11:
            this.TXT_ProjetNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.BTN_AddContactProjet = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_AddContactProjet.Click += new System.Windows.RoutedEventHandler(this.BTN_AddContactProjet_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.BTN_RemoveContactProjet = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_RemoveContactProjet.Click += new System.Windows.RoutedEventHandler(this.BTN_RemoveContactProjet_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.GRB_ProjetBddADG = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 15:
            this.LBL_ProjetADGBddNom = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.LBL_ProjetADGBddUserNom = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.LBL_ProjetADGBddPassword = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.TXT_ProjetADGBddNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.TXT_ProjetADGBddUserNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.TXT_ProjetADGBddPassword = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.GRB_ProjetBddClient = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 22:
            this.LBL_ProjetClientBddNom = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.LBL_ProjetClientBddUserNom = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.LBL_ProjetClientBddPassword = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.TXT_ProjetClientBddNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.TXT_ProjetClientBddUserNom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.TXT_ProjetClientBddPassword = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.LBL_ProjetClickOnce = ((System.Windows.Controls.Label)(target));
            return;
            case 29:
            this.TXT_ProjetClickOnce = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.LBL_ProjetGit = ((System.Windows.Controls.Label)(target));
            return;
            case 31:
            this.TXT_ProjetGit = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.CBX_ProjetPresenceBdd = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 33:
            this.DG_ContactProjet = ((System.Windows.Controls.DataGrid)(target));
            
            #line 82 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.DG_ContactProjet.AddingNewItem += new System.EventHandler<System.Windows.Controls.AddingNewItemEventArgs>(this.DG_ContactProjet_AddingNewItem);
            
            #line default
            #line hidden
            return;
            case 34:
            this.TXT_Redmine = ((System.Windows.Controls.TextBox)(target));
            
            #line 105 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.TXT_Redmine.LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            
            #line 105 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.TXT_Redmine.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 35:
            this.TXT_ProjetCommentaire = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            this.BTN_AcceptProjet = ((System.Windows.Controls.Button)(target));
            
            #line 114 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_AcceptProjet.Click += new System.Windows.RoutedEventHandler(this.BTN_AcceptProjet_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.BTN_CancelProjet = ((System.Windows.Controls.Button)(target));
            
            #line 115 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_CancelProjet.Click += new System.Windows.RoutedEventHandler(this.BTN_CancelProjet_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.DG_Assistance = ((System.Windows.Controls.DataGrid)(target));
            
            #line 118 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.DG_Assistance.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.DG_Assistance_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 39:
            this.BTN_DeleteAssistance = ((System.Windows.Controls.Button)(target));
            
            #line 137 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_DeleteAssistance.Click += new System.Windows.RoutedEventHandler(this.BTN_RemoveAssistance_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            this.BTN_ModifyAssistance = ((System.Windows.Controls.Button)(target));
            
            #line 138 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_ModifyAssistance.Click += new System.Windows.RoutedEventHandler(this.BTN_ModifyAssistance_Click);
            
            #line default
            #line hidden
            return;
            case 41:
            this.BTN_AddAssistance_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 140 "..\..\..\..\Vue\DataManagement\ManageProjet.xaml"
            this.BTN_AddAssistance_Copy.Click += new System.Windows.RoutedEventHandler(this.BTN_AddAssistance_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

