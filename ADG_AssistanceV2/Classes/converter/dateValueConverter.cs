﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ADG_Assistance.Converter
{

    public class DateValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null)
                return "";
            else
            {
                DateTime dtDate = (DateTime)value;
                return dtDate.ToString("dd/MM/yyyy");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                DateTime convertedDate = System.Convert.ToDateTime((string)value);
                return convertedDate;
            }
            catch (Exception)
            {

                return null;
            }
        }
    }


}
