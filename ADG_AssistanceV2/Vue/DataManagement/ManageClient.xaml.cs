﻿using ADG_Assistance.DB;
using ADG_Assistance.Functions;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageClient.xaml
	/// </summary>
	public partial class ManageClient : Window
	{
        #region "Données membres"
        private UIElement m_oParent;
		ADG_AssistanceEntities dbEntity; 
		private Client oClientSel;
		private Boolean clientIsModif;

		public Contact selectedContact { get; set; }
		private DisplayContact displayContact;
		MainWindow m_oWin;
		private bool isClosed = true;
        private bool isCancel = false;
        #endregion

        #region "Constructeur"
        public ManageClient(Client clientSel, Boolean isModif, String clientNom, MainWindow oWin, ADG_AssistanceEntities _dbEntity, UIElement oParent)
		{
			InitializeComponent();
			dbEntity = _dbEntity;
			oClientSel = clientSel;
			clientIsModif = isModif;
			m_oWin = oWin;
            m_oParent = oParent;
                

           displayContact = new DisplayContact(m_oWin);   

            DataContext = oClientSel;

			ToUpdate();
		}

        public ManageClient(Client clientSel, String clientNom, ADG_AssistanceEntities _dbEntity)
        {
            InitializeComponent();
            dbEntity = _dbEntity;
            oClientSel = clientSel;
            clientIsModif = false;
            m_oWin = null;


            displayContact = new DisplayContact(m_oWin);

            DataContext = oClientSel;

            ToUpdate();
        }

        public ManageClient(Client clientSel, Boolean isModif, string clientNom, string ville, string tel)
		{
			InitializeComponent();
			oClientSel = clientSel;
			clientIsModif = isModif;
            DataContext = oClientSel;
            m_oWin = null;


        }

        #endregion

        private void saveClient()
		{
			try
			{
				dbEntity.SaveChanges();
				ToUpdateParent();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Une erreur est survenue lors de la sauvegarde des données : " + ex.Message);
			}
		}
		

		private void BTN_SaveAddClient_Click(object sender, RoutedEventArgs e)
		{
			saveClient();
            isCancel = true;

            if (isClosed)
				Close();
		}

		private void BTN_CancelAddClient_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}


        /// <summary>
        /// Met à jour la fenêtre parent en déclenchant l'évènment Loaded
        /// </summary>
        public virtual void ToUpdateParent()
        {
            Function.declencheEvenementLoaded(m_oParent, this);                   
                
        }

        private void BTN_Modifier_Click(object sender, RoutedEventArgs e)
		{
			ModifyContactInClient();
		}

		private void BTN_Supprimer_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				Contact contactSelect = DG_Contact.SelectedItem as Contact;
				MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer le contact " + contactSelect.fullName + " de la société " + contactSelect.Client.Nom + " ?", "Alerte supression", MessageBoxButton.YesNo);
				if (dr == MessageBoxResult.Yes)
				{
					IQueryable<ContactProjet> lst = (from contratProjet in dbEntity.ContactProjet where contratProjet.IdContact == contactSelect.Id select contratProjet);

					if (!lst.Any())
					{
						dbEntity.Contact.Remove(contactSelect);
						dbEntity.SaveChanges();
						ToUpdate();
					}
					else
					{
						MessageBox.Show("Impossible de supprimer ce contact, il est lié à un projet, vérifiez la table ContactProjet");
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Une erreur est survenue lors de la suppression du contrat : " + ex.Message);
			}
		}

		public void ToUpdate()
		{
            var qContact = from contact in dbEntity.Contact where contact.IdClient == oClientSel.id select contact;
            DG_Contact.ItemsSource = qContact.ToList();
            DG_Contact.Items.Refresh();
		}


		private void DG_Contact_DoubleClick(object sender, RoutedEventArgs e)
		{
			ModifyContactInClient();
		}
		private void ModifyContactInClient()
		{			
			
            if (DG_Contact.SelectedItem != null)
            {
                displayContact.ToModif = true;
                selectedContact = (Contact)DG_Contact.SelectedItem;
                ManageContact contact = new ManageContact(displayContact, selectedContact, dbEntity, true, this);
                contact.Show();
            }
			
		}

		private void BTN_AjouterContact_Click(object sender, RoutedEventArgs e)
		{
			displayContact.ToModif = false;          
			ManageContact contact = new ManageContact(displayContact, selectedContact, dbEntity, true, this);
			contact.Show();
		}

        private void Window_Closed(object sender, EventArgs e)
        {
            if (!clientIsModif && !isCancel)
            {
                try
                {
                    Client client = oClientSel as Client;
                    dbEntity.Client.Remove(client);
                    dbEntity.SaveChanges();
                    ToUpdate();
                }
#pragma warning disable S2486 // Generic exceptions should not be ignored
                catch
#pragma warning disable S108 // Nested blocks of code should not be left empty
                {

                }
#pragma warning restore S108 // Nested blocks of code should not be left empty
#pragma warning restore S2486 // Generic exceptions should not be ignored

            }
        }
    }
}
