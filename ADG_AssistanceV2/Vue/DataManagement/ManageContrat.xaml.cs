﻿using ADG_Assistance.Functions;
using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Runtime.Caching;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageContrat.xaml
	/// </summary>
	public partial class ManageContrat : Window
	{

		DisplayContrat parentContrat;
		public Contrat oContratSel { get; set; }
		ADG_AssistanceEntities dbEntity;
		private Projet oProjetSel;
        private int keyProjet = 0;
		private Boolean initWindow;
        private Boolean isModif;
        private bool isCancel = false;
        private MainWindow m_oWin;
        private double heureRest = 0;
        private int contratDesactive = 0;
        ObjectCache cache = MemoryCache.Default;
        

        public ManageContrat(DisplayContrat contrat, ADG_AssistanceEntities dbEntityParent, MainWindow oWin)
        {
            InitializeComponent();
            dbEntity = dbEntityParent;
            m_oWin = oWin;

            oContratSel = new Contrat
            {

                NumContrat = GenerateContratNumber(),
                DateCreation = DateTime.Now,
                Cout = 0,
                NbHeures = 0,
                HeuresRestantes = 0,
                Contact = ""
            };

            dbEntity.Contrat.Add(oContratSel);
            dbEntity.SaveChanges();

            parentContrat = contrat;
            FillFields();
            initWindow = false;
		}

		public ManageContrat(DisplayContrat contact, Contrat selectedContrat, ADG_AssistanceEntities dbEntityParent, MainWindow oWin)
		{
			InitializeComponent();
			dbEntity = dbEntityParent;
			parentContrat = contact;
            m_oWin = oWin;
            oContratSel = selectedContrat;
			FillFields();
			initWindow = false;
            isModif = true;
            
            object roleConvert = cache.Get("Role");
            string role = roleConvert.ToString();
            if (role != "Administrateur")
            {
                this.BTN_AcceptContrat.Visibility = Visibility.Hidden;
                this.BTN_AddContratProjet.Visibility = Visibility.Hidden;
                this.BTN_AddProjetContact.Visibility = Visibility.Hidden;
                this.BTN_CancelContrat.Visibility = Visibility.Hidden;
                this.BTN_CancelProjetContact.Visibility = Visibility.Hidden;
                this.BTN_EditContrat.Visibility = Visibility.Hidden;
                
                this.BTN_ModifyContratProjet.Visibility = Visibility.Hidden;
                this.BTN_RemoveContratProjet.Visibility = Visibility.Hidden;
                this.UpDown_ContratCout.Visibility = Visibility.Hidden;
                this.LBL_ContratCout.Visibility = Visibility.Hidden;
            }


            BgColor();
        }

		private void FillFields()
		{
			if (oContratSel != null)
			{


                ToUpdate();

			}
		}

		private void BTN_AddContratProjet_Click(object sender, RoutedEventArgs e)
		{
            SelectProjet select = new SelectProjet(this, oContratSel, m_oWin, dbEntity, TXT_ContratClientNom.Text);
            select.ShowDialog();
            if (select.oProjetSel != null)
            {
                var reqContratExist = from projettocontrat in dbEntity.ProjetToContrat select projettocontrat.idProjet;
                keyProjet = select.oProjetSel.Id;
                bool projetExist = false;
                int lastContratId = 0;
                foreach (var verif in reqContratExist)
                {
                    if (verif == keyProjet)
                    {
                        projetExist = true;
                        lastContratId = (int) verif;
                    }
                    
                }

                var reqContrat = from projettocontrat in dbEntity.ProjetToContrat
                                      where projettocontrat.idProjet == lastContratId
                                      select projettocontrat.idContrat;

                int numContrat = 0;

                foreach(var verif in reqContrat)
                {
                    numContrat = (int) verif;
                }

                contratDesactive = numContrat;

                var reqContratActif = from projettocontrat in dbEntity.ProjetToContrat
                              join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                              where contrat.Id == numContrat
                              select contrat.DateFin;

                DateTime dateFinContrat = DateTime.Today;

                foreach(var verif in reqContratActif)
                {
                    dateFinContrat = (DateTime)verif;
                }

                var reqContratHeure = from projettocontrat in dbEntity.ProjetToContrat
                                      join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                      where contrat.Id == numContrat
                                      select contrat.HeuresRestantes;

                double heureRestant = 0;

                foreach(var verif in reqContratHeure)
                {
                    heureRestant = (double)verif;
                }



                int resultDateCompare = DateTime.Compare(dateFinContrat, DateTime.Today);

                 if (projetExist && (resultDateCompare > 0))
                {
                    if ((MessageBox.Show("Un contrat existe déjà pour ce projet, voulez-vous quand même le créer ?", "Informations", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes))
                    {
                        if (heureRestant < 0)
                        {
                            if ((MessageBox.Show("L'ancien contrat avait des heures restantes négatives, elles seront soustraits aux heures restantes de ce contrat.", "Informations", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes))
                            {
                                select.oProjetSel.IdContrat = oContratSel.Id;
                                heureRest = heureRestant;
                                dbEntity.SaveChanges();
                                
                            }
                            
                        }
                        else
                        {
                            select.oProjetSel.IdContrat = oContratSel.Id;
                            heureRest = heureRestant;
                            dbEntity.SaveChanges();
                        }
                    }

                        
                }
                else
                {
                    
                    select.oProjetSel.IdContrat = oContratSel.Id;
                    dbEntity.SaveChanges();
                }
                
                
            }

            ToUpdate();
        }

		private void BTN_RemoveContratProjet_Click(object sender, RoutedEventArgs e)
		{
			oProjetSel = (Projet)DG_contratProjets.SelectedItem;

			if (oProjetSel != null)
			{
                String szString = "Le projet '" + oProjetSel.Nom + " ne sera plus lié au contrat '" + oContratSel.NumContrat + "'. Voulez-vous continuer ?";

                if (MessageBox.Show(szString, "Contrat", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    oProjetSel.IdContrat = null;
                    dbEntity.SaveChanges();

                    grpClient.DataContext = null;
                    grpClient.DataContext = oContratSel;
                    
                }

            }

			ToUpdate();
		}				

		private void BTN_AddProjetContact_Click(object sender, RoutedEventArgs e)
		{
            dbEntity.SaveChanges();
            ToUpdate();		
		}

		private void BTN_CancelProjetContact_Click(object sender, RoutedEventArgs e)
		{
			HideAddProjet();
		}

		public void HideAddProjet()
		{
			TXT_ContratProjetNom.Text = "";
			CBX_ContratProjetPresenceBase.IsChecked = false;
			GRB_ContratProjet.Visibility = Visibility.Hidden;
		}

		private void ToUpdate()
		{

            //--- Permet de forcer la mise à jour ---//
            DataContext = null;
            grpClient.DataContext = null;
            TXT_ContratContactNom.DataContext = null;
            CMB_ContratClientNom.DataContext = null;
            CBB_ContratContactCivilite.DataContext = null;

            var reqCout = from contratdb in dbEntity.Contrat
                          where contratdb.NumContrat == oContratSel.NumContrat
                          select contratdb.Cout;

            int cout_contrat = 0;

            foreach (var verif in reqCout)
            {
                cout_contrat = (int)verif;
            }

            UpDown_ContratCout.Value = cout_contrat;

            DataContext = oContratSel;
            //grpClient.DataContext = oContratSel.client;
            TXT_ContratContactNom.DataContext = oContratSel;
            CMB_ContratClientNom.DataContext = oContratSel;
            CBB_ContratContactCivilite.DataContext = oContratSel;

            var qProjet = from projet in dbEntity.Projet
                          join contrat in dbEntity.Contrat on projet.IdContrat equals contrat.Id
                          where contrat.Id == oContratSel.Id
                          select projet;

            DG_contratProjets.ItemsSource = qProjet.ToList();
			DG_contratProjets.Items.Refresh();
		}

		private void BTN_AcceptContrat_Click(object sender, RoutedEventArgs e)
		{

            isCancel = true;

            if(Date_ContratDateDeb.SelectedDate == null || Date_ContratDateFin.SelectedDate == null || Date_ContratDateSignature.SelectedDate == null || UpDown_ContratCout.Value == 0)
            {
                MessageBox.Show("Les champs requis d'une * sont obligatoires !");
            }
            else
            {
                SaveContrat(true);

                if (!isModif)
                {

                    var connectionString = Function.connectDatabase();
                    SqlConnection con = new SqlConnection(connectionString);
                    try
                    {
                        con.Open();
                        SqlCommand reqUpdateHeure = new SqlCommand();
                        reqUpdateHeure.Connection = con;
                        reqUpdateHeure.CommandText = "UPDATE Contrat SET Actif = @actif WHERE Id = @id";
                        reqUpdateHeure.Parameters.AddWithValue("@id", contratDesactive);
                        reqUpdateHeure.Parameters.AddWithValue("@actif", 0);

                        reqUpdateHeure.ExecuteNonQuery();

                    }
                    catch
                    {

                    }

                    oContratSel.Actif = true;
                    oContratSel.HeuresRestantes = oContratSel.NbHeures + heureRest;

                    ProjetToContrat projetToContrat = new ProjetToContrat
                    {
                        idContrat = oContratSel.Id,
                        idProjet = keyProjet
                    };
                    dbEntity.ProjetToContrat.Add(projetToContrat);
                    dbEntity.SaveChanges();
                    parentContrat.ToUpdate();

                    
                }
            }
			
            
        }

		private void BTN_CancelContrat_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}


		private string GenerateContratNumber()
		{
            int max = 0;
            String toReturn = "CM";
            StringBuilder strBuilder = new StringBuilder(toReturn);

            var qContrat = from contrat in dbEntity.Contrat
                           where contrat.NumContrat.StartsWith(toReturn)
                           select contrat.NumContrat;

			

			foreach (String numContrat in qContrat)
			{
                string[] splitString = numContrat.Split('M');
				int lastInt = int.Parse(splitString.Last());
				if (max <= lastInt)
				{
					max = lastInt;
				}
			}

			max += 1;

			for (int i = 5 - max.ToString().Length; i > 0; i--)
			{
                strBuilder.Append("0");
                
			}

            strBuilder.Append(max.ToString());


            return strBuilder.ToString();	
		}

		private void SaveContrat(bool closeContrat)
		{
            oContratSel.Cout = (int)UpDown_ContratCout.Value;
            dbEntity.SaveChanges();

			if(closeContrat)
			{

                parentContrat.ToUpdate();
                this.Close();
                
            }
		}

		private void BTN_ModifyContratProjet_Click(object sender, RoutedEventArgs e)
		{

			oProjetSel = (Projet)DG_contratProjets.SelectedItem;

			if (oProjetSel != null)
			{
				ModifyContactProjet(oProjetSel);
			}
			else
			{
				MessageBox.Show("Merci de sélectionner une et une seule colonne à modifier");
			}
			ToUpdate();

		}

		private void ModifyContactProjet(Projet oProjetSel)
		{
			GRB_ContratProjet.Visibility = Visibility.Visible;
			CBX_ContratProjetPresenceBase.IsChecked = oProjetSel.PresenceBase;
			TXT_ContratProjetNom.Text = oProjetSel.Nom;
			parentContrat.isModif = true;
		}

		private void BTN_EditContrat_Click(object sender, RoutedEventArgs e)
		{
            SaveContrat(false);
            MessageBoxResult dr = MessageBox.Show("Voulez-vous générer un fichier word pour le contrat n° " + oContratSel.NumContrat  + "?\n\nChemin de sauvegarde: \\ADGDAGOBAH/ADG-Assistance/Documents/Contrats", "Alerte génération", MessageBoxButton.YesNo , MessageBoxImage.Information);

            if (dr == MessageBoxResult.Yes)
            {

                EditWord word = new EditWord(oContratSel.NumContrat, oContratSel);
                parentContrat.isModif = true;
            }
		}

        private void CMB_ContratClientNom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CMB_ContratClientNom.SelectedItem != null)
            {
                TXT_ContratClientNom.DataContext = null;
                TXT_ContratClientNom.DataContext = CMB_ContratClientNom.SelectedItem;
                grpClient.DataContext = null;
                grpClient.DataContext = CMB_ContratClientNom.SelectedItem;

                TXT_ContratContactNom.DataContext = null;
                CMB_ContratClientNom.DataContext = null;
                TXT_ContratContactNom.DataContext = oContratSel;                
                CMB_ContratClientNom.DataContext = oContratSel;
    
            }
        }

        private void TXT_ContratClientNom_KeyUp(object sender, KeyEventArgs e)
        {
            if (!initWindow)
            {
                if (CMB_ContratClientNom.Text != TXT_ContratClientNom.Text || TXT_ContratClientNom.Text == "")
                {
                    CMB_ContratClientNom.Items.Clear();
                }

                dbEntity.Client.Load();
                var clients = dbEntity.Client.Local;

                foreach (Client client in clients)
                {
                    if (client.Nom.ToLower().StartsWith(TXT_ContratClientNom.Text.ToLower()))
                    {
                        CMB_ContratClientNom.Items.Add(client);
                    }
                }

                if (CMB_ContratClientNom.Items.Count != 0)
                {
                    CMB_ContratClientNom.IsDropDownOpen = true;
                }
                else
                {
                    CMB_ContratClientNom.IsDropDownOpen = false;
                }
            }
        }

        private void DG_contratProjets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG_contratProjets.SelectedItem != null)
            {
                grpClient.DataContext = null;
                grpClient.DataContext = ((Projet)DG_contratProjets.SelectedItem).Client;
            }
        }

        private void BgColor()
        {
            if((bool)oContratSel.Actif)
            {
                this.Background = new BrushConverter().ConvertFromString("#FF90EE90") as SolidColorBrush;
            }
            else
            {
                this.Background = new BrushConverter().ConvertFromString("#FFD3D3D3") as SolidColorBrush;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (!isModif && !isCancel)
            {
                try
                {
                    Contrat contrat = oContratSel as Contrat;
                    dbEntity.Contrat.Remove(contrat);
                    dbEntity.SaveChanges();
                    ToUpdate();
                }
#pragma warning disable S2486 // Generic exceptions should not be ignored
                catch
#pragma warning disable S108 // Nested blocks of code should not be left empty
                {

                }
#pragma warning restore S108 // Nested blocks of code should not be left empty
#pragma warning restore S2486 // Generic exceptions should not be ignored

            }
        }
    }
}
