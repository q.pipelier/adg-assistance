﻿using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageContact.xaml
	/// </summary>
	public partial class ManageContact : Window
	{

		ADG_AssistanceEntities dbEntity;
		private DisplayContact displayContact;
		private SelectContact selectContact;
		
		private Contact selectedContact;
		private Boolean ToModif = false;
		private Boolean isDisplayContact = false;
		private Boolean isInManageClient;
		private ManageClient client;
        private Boolean isSelected = false;

		/// <summary>
		/// From DisplayContact
		/// </summary>
		/// <param name="parentContact"></param>
		/// <param name="contactSel"></param>
		public ManageContact(DisplayContact parentContact, Object contactSel, ADG_AssistanceEntities _dbentity, Boolean isManageClient, ManageClient manageClient)
		{
			InitializeComponent();
			dbEntity = _dbentity;
			isDisplayContact = true;
			selectedContact = (Contact)contactSel;
			isInManageClient = isManageClient;
			client = manageClient;

			var qProjet = from projet in dbEntity.Projet
						  join contact_projet in dbEntity.ContactProjet on projet.Id equals contact_projet.IdProjet
						  join contact in dbEntity.Contact on contact_projet.IdContact equals contact.Id
						  where contact.Id == selectedContact.Id
						  orderby projet.Nom
						  select projet;

			displayContact = parentContact;
			if (displayContact.ToModif)
			{
				ToModif = true;
				GetDataContact(selectedContact);
				DataGridContactProjet.ItemsSource = qProjet.ToList();
			}
		}

        public ManageContact(Object contactSel, ADG_AssistanceEntities _dbentity, Boolean isManageClient, String societe)
        {
            InitializeComponent();
            dbEntity = _dbentity;
            isDisplayContact = true;
            selectedContact = (Contact)contactSel;
            isInManageClient = isManageClient;
            TXT_ContactClientNom.Text = societe;
            client = null;
            isSelected = true;
        }

        /// <summary>
        /// From selectContact
        /// </summary>
        /// <param name="contactList"></param>
        /// <param name="contactSel"></param>
        public ManageContact(SelectContact contactList, Object contactSel, ADG_AssistanceEntities _dbentity, Boolean isManageClient, ManageClient manageClient)
		{
			dbEntity = _dbentity;
			InitializeComponent();

			selectedContact = (Contact)contactSel;

			selectContact = contactList;
			var qProjet = from projet in dbEntity.Projet
						  join contact_projet in dbEntity.ContactProjet on projet.Id equals contact_projet.IdProjet
						  join contact in dbEntity.Contact on contact_projet.IdContact equals contact.Id
						  where contact.Id == selectedContact.Id
						  orderby projet.Nom
						  select projet;

			if (selectContact.ToModif)
			{
				ToModif = true;
				GetDataContact(selectedContact);
				DataGridContactProjet.ItemsSource = qProjet.ToList();
			}
		}

		private void BTN_AddContactOk_Click(object sender, RoutedEventArgs e)
		{
			SaveContact();
		}

		public void BTN_CancelContact_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// TODO Projet / Client => Very Important !!!!
		/// </summary>
		private void SaveContact()
		{
			if (TXT_ContactClientNom.Text == "" || TXT_ContactMail.Text == "" || TXT_ContactNom.Text == "" || TXT_ContactPrenom.Text == "")
			{
				MessageBox.Show("Merci de remplir les champs précédés d'un '*'");
			}
			else
			{

                bool nomClient = false;
       

                if (TXT_ContactClientNom.Text != "")
                {
                    var nomEntreprise = from client in dbEntity.Client
                                        select client.Nom;

                    foreach (var item in nomEntreprise)
                    {
                        if (TXT_ContactClientNom.Text == item)
                        {

                            nomClient = true;
                        }


                    }
                }
                if (nomClient == false)
                {
                    MessageBox.Show("Merci d'entrer un nom de societe correct.");
                }
                else
                {
                    if (!ToModif)
                    {
                        var contact = dbEntity.Set<Contact>();
                        contact.Add(new Contact
                        {

                            Civilite = ((ComboBoxItem)CMB_ContactCivilite.SelectedItem).Content.ToString(),
                            Fax = TXT_ContactFax.Text,
                            Fonction = TXT_ContactFonction.Text,
                            Mail = TXT_ContactMail.Text,
                            Nom = TXT_ContactNom.Text,
                            Portable = TXT_ContactTelPortable.Text,
                            Prenom = TXT_ContactPrenom.Text,
                            Telephone = TXT_ContactTelPortable.Text,
                            Commentaire = TXT_ContactCommentaire.Text,
                            IdClient = ((Client)TXT_ContactClientNom.DataContext).id
                        });
                    }
                    else
                    {


                        selectedContact.Civilite = CMB_ContactCivilite.Text;
                        selectedContact.IdClient = ((Client)TXT_ContactClientNom.DataContext).id;
                        selectedContact.Commentaire = TXT_ContactCommentaire.Text;
                        // TODO : contactSelected.ContactProjet = [...]
                        selectedContact.Fax = TXT_ContactFax.Text;
                        selectedContact.Fonction = TXT_ContactFonction.Text;
                        selectedContact.Mail = TXT_ContactMail.Text;
                        selectedContact.Nom = TXT_ContactNom.Text;
                        selectedContact.Portable = TXT_ContactTelPortable.Text;
                        selectedContact.Prenom = TXT_ContactPrenom.Text;
                        selectedContact.Telephone = TXT_ContactTelFixe.Text;
                    }
                    dbEntity.SaveChanges();
                    if (!isSelected)
                    {
                        if (isDisplayContact)
                        {
                            displayContact.ToUpdate();
                        }
                        else
                        {
                            selectContact.toUpdate();
                        }
                    }
                    
                    this.Close();
                }

               
			}
		}

		public void GetDataContact(Contact contact)
		{
			TXT_ContactClientNom.DataContext = contact.Client;
			TXT_ContactCommentaire.Text = contact.Commentaire;
			TXT_ContactFax.Text = contact.Fax;
			TXT_ContactFonction.Text = contact.Fonction;
			TXT_ContactMail.Text = contact.Mail;
			TXT_ContactNom.Text = contact.Nom;
			TXT_ContactPrenom.Text = contact.Prenom;
			TXT_ContactTelFixe.Text = contact.Telephone;
			TXT_ContactTelPortable.Text = contact.Portable;
		}

		private void TXT_ContactClientNom_KeyUp(object sender, KeyEventArgs e)
		{
			if (CMB_ContactClientNom.Text != TXT_ContactClientNom.Text || TXT_ContactClientNom.Text == "")
			{
				CMB_ContactClientNom.Items.Clear();
			}

			CMB_ContactClientNom.IsDropDownOpen = true;
			dbEntity.Client.Load();
			var clients = dbEntity.Client.Local;

			foreach (Client clt in clients)
			{
				if (clt.Nom.ToLower().StartsWith(TXT_ContactClientNom.Text.ToLower()))
				{
					CMB_ContactClientNom.Items.Add(clt);
				}
			}
		}

		private void CMB_ContactClientNom_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TXT_ContactClientNom.DataContext = null;
			TXT_ContactClientNom.DataContext = CMB_ContactClientNom.SelectedItem;
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			if (isInManageClient)
				client.ToUpdate();
		}
	}
}