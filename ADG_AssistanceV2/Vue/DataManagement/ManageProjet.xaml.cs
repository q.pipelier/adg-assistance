﻿using ADG_Assistance.DB;
using ADG_Assistance.Functions;
using ADG_Assistance.Vue.DataDisplay;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Navigation;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageProjet.xaml
	/// </summary>
	public  partial class ManageProjet : Window
	{
		public Boolean projetisModif { get; set; }
        readonly Logger Log = LogManager.GetCurrentClassLogger();

        ADG_AssistanceEntities parentDbEntity;
		public Projet selectedProjet { get; set; }
        UIElement m_oParent;
		List<Contact> listContact = new List<Contact>();		
		List<Contact> toAddListContact = new List<Contact>();
        private List<Assistance> listAssistance = new List<Assistance>();
        private bool isClose = false;

		MainWindow m_oWin;

		public ManageProjet(Projet projet, Boolean isModif, ADG_AssistanceEntities dbContext, MainWindow oWin, UIElement oParent)
		{
			InitializeComponent();
			projetisModif = isModif;
			selectedProjet = projet;
			parentDbEntity = dbContext;
            m_oParent = oParent;
            m_oWin = oWin;
            DataContext = selectedProjet;

            if (isModif)
            {
                var reqClient = from projetdb in parentDbEntity.Projet
                                 join client in parentDbEntity.Client on projetdb.IdClient equals client.id
                                 where projetdb.IdClient == projet.IdClient
                                 select client.Nom;

                string client_name = "";

                foreach (var verif in reqClient)
                {
                    client_name = (string)verif;
                }

                TXT_NomClient.Text = client_name;
            }
            
		}

        private void BTN_AcceptProjet_Click(object sender, RoutedEventArgs e)
		{
			saveProjet();
		}

		private void BTN_CancelProjet_Click(object sender, RoutedEventArgs e)
		{
            if (!projetisModif)
            {
                Projet projet = selectedProjet;
                parentDbEntity.Assistance.RemoveRange(projet.Assistance);
                parentDbEntity.ContactProjet.RemoveRange(projet.ContactProjet);
                parentDbEntity.Projet.Remove(projet);
            }
            
            parentDbEntity.SaveChanges();
            ToUpdate();
            isClose = true;
            this.Close();
		}

		private void BTN_RemoveContactProjet_Click(object sender, RoutedEventArgs e)
		{
			var selectedItem = DG_ContactProjet.SelectedItem;
			if (selectedItem != null)
			{
				try
				{
					Contact oContactProjetSel = (Contact)selectedItem;
					var qDelete = from contact in parentDbEntity.Contact
								  join contact_projet in parentDbEntity.ContactProjet on contact.Id equals contact_projet.IdContact
								  join projet in parentDbEntity.Projet on contact_projet.IdProjet equals projet.Id
								  where contact.Id == oContactProjetSel.Id
								  where projet.Id == selectedProjet.Id
								  select contact_projet;

					foreach (var item in qDelete)
					{
						parentDbEntity.ContactProjet.Remove(item);
					}
					parentDbEntity.SaveChanges();
					ToUpdate();
				}
				catch (Exception ex )
				{
                    Function.AfficheException(ex, "Une erreur est survenue lors de la suppression du contact du projet : ", "Projet", Log);
				}
			}
		}

		private void BTN_AddContactProjet_Click(object sender, RoutedEventArgs e)
		{
			SelectContact contact = new SelectContact(this);
			contact.Show();
        }



		public void saveProjet()
		{

            bool nomClient = false;
            bool nomProjet = true;

                if(TXT_NomClient.Text != "")
                {
                    var nomEntreprise = from client in parentDbEntity.Client
                    select client.Nom;

                    foreach(var item in nomEntreprise)
                     {
                        if(TXT_NomClient.Text == item)
                        {
                        
                            nomClient = true;

                        }
                   
                   
                    }
                }

            if (TXT_ProjetNom.Text == "")
            {
                nomProjet = false;
            }

        

                if(nomClient == false)
            {
                MessageBox.Show("Merci d'entrer un nom de societe correct.");
            }
            else
            {
                if (nomProjet == false)
                {
                    Projet projet = selectedProjet as Projet;
                    parentDbEntity.Assistance.RemoveRange(projet.Assistance);
                    parentDbEntity.ContactProjet.RemoveRange(projet.ContactProjet);
                    parentDbEntity.Projet.Remove(projet);
                    parentDbEntity.SaveChanges();
                    MessageBox.Show("Merci d'entrer un nom de projet correct.");
                }
            }

                if(nomProjet == true && nomClient == true)
            {
                selectedProjet.IdClient = ((Client)TXT_NomClient.DataContext).id;
                isClose = true;
                parentDbEntity.SaveChanges();
                ToUpdateParent();
                this.Close();
                
            }

		}

		public void fillFields()
        {
			Projet oProjetSel = selectedProjet;
			
			ToUpdate();
            this.DataContext = null;
            TXT_NomClient.DataContext = null;

            this.DataContext = oProjetSel;
            TXT_NomClient.DataContext = oProjetSel.Client;
        }

		public void ToUpdate()
		{
			Projet oProjetSel = selectedProjet;

			listContact.Clear();
            listAssistance.Clear();

			listContact = (from contact in parentDbEntity.Contact
						   join contact_projet in parentDbEntity.ContactProjet on contact.Id equals contact_projet.IdContact
						   join projet in parentDbEntity.Projet on contact_projet.IdProjet equals projet.Id
						   where projet.Id == oProjetSel.Id
						   orderby contact.Nom
						   select contact).ToList();

			

            listAssistance = (from assistance in parentDbEntity.Assistance
                              join projet in parentDbEntity.Projet on assistance.idProjet equals projet.Id
                              where projet.Id == oProjetSel.Id
                              orderby assistance.nom
                              select assistance).ToList();

			DG_ContactProjet.ItemsSource = listContact;
            DG_Assistance.ItemsSource = listAssistance;
		}

        public void ToUpdateParent()
        {
            Function.declencheEvenementLoaded(m_oParent, this);
        }


        private void DG_ContactProjet_AddingNewItem(object sender, AddingNewItemEventArgs e)
		{
			foreach (Contact contact in DG_ContactProjet.Items)
			{
				if (!listContact.Contains(contact))
				{
					listContact.Add(contact);
					toAddListContact.Add(contact);
				}
			}
			ToUpdate();
		}

	

        private void CMB_NomClient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CMB_NomClient.SelectedItem != null)
            {
                TXT_NomClient.DataContext = null;
                TXT_NomClient.DataContext = CMB_NomClient.SelectedItem;
            }
        }

        private void TXT_NomClient_KeyUp(object sender, KeyEventArgs e)
        {
            CMB_NomClient.Items.Clear();
            CMB_NomClient.IsDropDownOpen = true;
            parentDbEntity.Client.Load();
            var clients = parentDbEntity.Client.Local;

            foreach (Client client in clients)
            {
                if (client.Nom.ToLower().StartsWith(TXT_NomClient.Text.ToLower()))
                {
                    CMB_NomClient.Items.Add(client);
                }
            }

            if (CMB_NomClient.Items.Count == 0)
            {
                CMB_NomClient.IsDropDownOpen = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            

            if (projetisModif)
            {
                var qparam = from param in parentDbEntity.Parametre select param.LienRedMine;

                selectedProjet.UrlRedmine = qparam.FirstOrDefault();
                fillFields();
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            try
            {
                Projet Projet = (Projet)DataContext;
                if (!String.IsNullOrEmpty(Projet.NomProjetRedmine))
                {
                    Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                }
            }
            catch (Exception)
            {
                MessageBox.Show("L'url " + e.Uri + " n'est pas accessible. ");
                
            }
            e.Handled = true;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            parentDbEntity.SaveChanges();
            fillFields();
        }

        private void BTN_AddAssistance_Click(object sender, RoutedEventArgs e)
        {
            ManageAssistance assistance = new ManageAssistance(this, projetisModif);
            assistance.Show();
        }

        private void BTN_RemoveAssistance_Click(object sender, RoutedEventArgs e)
        {
            if(DG_Assistance.SelectedItem == null)
            {
                MessageBox.Show("Merci de choisir une assistance à supprimer");
            }
            else
            {
                Assistance assistance = DG_Assistance.SelectedItem as Assistance;
                MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer l'assistance " + assistance.nom + " ?", "Alerte supression", MessageBoxButton.YesNo);
                if (dr == MessageBoxResult.Yes)
                {
                    parentDbEntity.Assistance.Remove(assistance);
                    parentDbEntity.SaveChanges();
                    ToUpdate();
                }
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void DG_Assistance_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!projetisModif && !isClose)
            {
                try
                {
                    Projet projet = selectedProjet as Projet;
                    parentDbEntity.Assistance.RemoveRange(projet.Assistance);
                    parentDbEntity.ContactProjet.RemoveRange(projet.ContactProjet);
                    parentDbEntity.Projet.Remove(projet);
                    parentDbEntity.SaveChanges();
                    ToUpdate();
                }
#pragma warning disable S2486 // Generic exceptions should not be ignored
                catch
#pragma warning disable S108 // Nested blocks of code should not be left empty
                {

                }
#pragma warning restore S108 // Nested blocks of code should not be left empty
#pragma warning restore S2486 // Generic exceptions should not be ignored

            }
            
        }

        private void BTN_ModifyAssistance_Click(object sender, RoutedEventArgs e)
        {
            Assistance assistance = DG_Assistance.SelectedItem as Assistance;
            if(assistance == null)
            {
                MessageBox.Show("Merci de sélectionner une ligne du tableau à modifier");
            }
            else
            {
                ManageAssistance manageAssistance = new ManageAssistance(this, assistance, true);
                manageAssistance.Show();
            }
           
        }
    }
}