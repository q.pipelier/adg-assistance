﻿using ADG_Assistance.Functions;
using ADG_Assistance.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageBI.xaml
	/// </summary>0
	public abstract partial class ManageBI : Window
	{
		private ADG_AssistanceEntities dbEntity;
		private BonIntervention oBonSel;
		private LigneIntervention oLISel;
		private IQueryable<LigneIntervention> qli;
		private Boolean parentIsModif = false;
		private List<LigneIntervention> listLI = new List<LigneIntervention>();
		private List<LigneIntervention> toAddListLI = new List<LigneIntervention>();
		private DateTime dateBI = DateTime.Now;
		private BonIntervention wordBonSel;
		private bool isLIModif;
		private bool isBIValid;

		protected ManageBI(BonIntervention bi, Boolean isModif, ADG_AssistanceEntities _dbEntity)
		{
			InitializeComponent();
			dbEntity = _dbEntity;
			LBL_Date.Content = DateTime.Now.ToString();
			CBB_NomClient.ItemsSource = (from client in dbEntity.Client select client.Nom).ToList();
			CBB_Personnel.ItemsSource = (from membres in dbEntity.Membres select membres.FullName).ToList();
			CBB_Intervenant.ItemsSource = (from membres in dbEntity.Membres select membres.FullName).ToList();
			Date_DateLI.Text = DateTime.Now.ToString();
			oBonSel = bi;
			

			if (isModif)
			{
				listLI = (from li in dbEntity.LigneIntervention where li.IdBon == oBonSel.Id select li).ToList();
				FillFieldsModif();
				parentIsModif = true;
			}
			else
			{
				LBL_NumBI.Content = GenerateContratNumber();
				LBL_Date.Content = DateTime.Now.ToLongDateString();
			}
			ToUpdateLI();
		}

		private string GenerateContratNumber()
		{
			var qBon = from bon in dbEntity.BonIntervention
					   select bon.NumeroBon;

			int max = 0;
			String toReturn = "BI";
            StringBuilder strBUild = new StringBuilder();
            strBUild.Append(toReturn);

			foreach (String numBon in qBon)
			{
				string[] splitString = numBon.Split('I');
				int lastInt = int.Parse(splitString.Last());
				if (max <= lastInt)
				{
					max = lastInt;
				}
			}

			max += 1;

			for (int i = 5 - max.ToString().Length; i > 0; i--)
			{
                strBUild.Append("0");
			}
            strBUild.Append(max.ToString());
            return strBUild.ToString();
		}

		public abstract void ParentToUpdate();

		private void TXT_NomClient_KeyUp(object sender, KeyEventArgs e)
		{
			if (CBB_NomClient.Text != TXT_NomClient.Text || TXT_NomClient.Text == "")
			{
				CBB_NomClient.Items.Clear();
			}
			else
			{
				FillFieldsLinkToClientNom();
			}

			CBB_NomClient.IsDropDownOpen = true;
			dbEntity.Client.Load();
			var clients = dbEntity.Client.Local;

			foreach (Client client in clients)
			{
				if (client.Nom.ToLower().StartsWith(TXT_NomClient.Text.ToLower()))
				{
					CBB_NomClient.Items.Add(client.Nom);
				}
			}

			if (CBB_NomClient.Items.Count == 0)
			{
				CBB_NomClient.IsDropDownOpen = false;
			}
		}

		private void CBX_OutOfContrat_Checked(object sender, RoutedEventArgs e)
		{
			CBB_NumContrat.IsEnabled = false;
			GRB_CreateClient.Visibility = Visibility.Visible;
			CBB_NumContrat.Text = "Hors Contrat";
		}

		private void CBX_OutOfContrat_Unchecked(object sender, RoutedEventArgs e)
		{
			CBB_NumContrat.IsEnabled = true;
			GRB_CreateClient.Visibility = Visibility.Hidden;
			if(parentIsModif && oBonSel.Contrat != null)
				CBB_NumContrat.Text = oBonSel.Contrat.NumContrat;
		}

		private void DG_LI_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			ModifyLI();
		}

		private void FillFieldsModif()
		{
			CBB_NomClient.SelectedItem = oBonSel.Client.Nom;
			CBB_Personnel.Text = oBonSel.Interlocuteur;
			CBX_OutOfContrat.IsChecked = oBonSel.HorsContrat;
			if(oBonSel.Contrat != null && oBonSel.Contrat.NumContrat != "")
				CBB_NumContrat.Text = oBonSel.Contrat.NumContrat;

            var qClient = from client in dbEntity.Client where client.Nom == TXT_NomClient.Text select client;
            if (qClient.Any())
            {
                Client fillClient = qClient.First();
                TXT_ClientTel.Text = fillClient.Numero;
                TXT_ClientVille.Text = fillClient.Ville;
            }

			LBL_Date.Content = oBonSel.Date.ToLongDateString();
			dateBI = oBonSel.Date;
			LBL_NumBI.Content = oBonSel.NumeroBon;
			FillFieldsLinkToClientNom();
			ToUpdateLI();
		}

		private void BTN_AcceptLI_Click(object sender, RoutedEventArgs e)
		{
			if (TXT_LITitre.Text == "" || CBB_Intervenant.Text == "" || UpDown_NombreHeures.Value == 0 || Date_DateLI.Text == "")
			{
				MessageBox.Show("Merci de remplir les champs précédés d'un '*'");
			}
			else if(!isLIModif)
			{
				LigneIntervention li;
                DateTime date = (DateTime) Date_DateLI.SelectedDate;

				if (CBB_NomProjet.Text == "")
				{
					li = new LigneIntervention
					{
						DateIntervention = date,
						Titre = TXT_LITitre.Text,
						Intervenant = CBB_Intervenant.Text,
						NbHeures = (int)UpDown_NombreHeures.Value,
						Description = TXT_Description.Text,
					};
				}
				else
				{
                    li = new LigneIntervention
					{
						DateIntervention = date,
						Titre = TXT_LITitre.Text,
						Intervenant = CBB_Intervenant.Text,
						NbHeures = (int)UpDown_NombreHeures.Value,
						Description = TXT_Description.Text,
						Projet = (Projet)CBB_NomProjet.SelectedItem
					};
				}
				listLI.Add(li);
				toAddListLI.Add(li);
			}
			else
			{
				LigneIntervention li = (LigneIntervention)DG_LI.SelectedItem;
				li.Titre = TXT_LITitre.Text;
				li.Intervenant = CBB_Intervenant.Text;
				li.NbHeures = (int)UpDown_NombreHeures.Value;
				li.Description = TXT_Description.Text;
			}

			HideManageLI();
			ToUpdateLI();
		}

		private void BTN_CancelLI_Click(object sender, RoutedEventArgs e)
		{
			ClearFields();
			HideManageLI();
		}

		private void BTN_AddLI_Click(object sender, RoutedEventArgs e)
		{
            if(CBB_NumContrat.Text == "" && CBX_OutOfContrat.IsChecked == false)
            {
                MessageBox.Show("Veuillez choisir un contrat avant.");
            }
            else
            {
                var reqDateFin = from contrat in dbEntity.Contrat
                                 where contrat.NumContrat == CBB_NumContrat.Text
                                 select contrat.Actif;

                bool actif = false;
                foreach(var result in reqDateFin)
                {
                    actif = (bool)result;
                }

                if(CBX_OutOfContrat.IsChecked == false)
                {
                    if (actif)
                    {
                        isLIModif = false;
                        ClearFields();
                        ShowManageLI();
                    }
                    else
                    {
                        MessageBox.Show("Le contrat " + CBB_NumContrat.Text + " n'est plus actif, il est impossible d'intervenir dessus.");
                    }
                }
                else
                {
                    isLIModif = false;
                    ClearFields();
                    ShowManageLI();
                }
                
                
                
            }
			
		}

		private void BTN_ModifyLI_Click(object sender, RoutedEventArgs e)
		{
			
			ModifyLI();
		}

		private void BTN_DeleteLI_Click(object sender, RoutedEventArgs e)
		{
			DeleteLI();
		}

		private void BTN_Accept_Click(object sender, RoutedEventArgs e)
		{
			SaveBI();
			if(isBIValid)
				Close();
		}

		private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void ModifyLI()
		{
            isLIModif = true;
            oLISel = (LigneIntervention)DG_LI.SelectedItem;
			if (oLISel != null)
			{
				TXT_Description.Text = oLISel.Description;
				TXT_LITitre.Text = oLISel.Titre;
				Date_DateLI.Text = oLISel.DateIntervention.ToString();
				UpDown_NombreHeures.Value = (int)oLISel.NbHeures;
				CBB_Intervenant.Text = oLISel.Intervenant;
				
                if(oLISel.Projet != null)
					CBB_NomProjet.Text = oLISel.Projet.Nom;
				
				ShowManageLI();
			}
			else
			{
				MessageBox.Show("Merci de sélectionner une ligne d'intervention");
			}

		}

		private void ShowManageLI()
		{
			GRB_ManageLI.Visibility = Visibility.Visible;
			DG_LI.Height = 229;
		}

		private void HideManageLI()
		{
			GRB_ManageLI.Visibility = Visibility.Hidden;
			DG_LI.Height = 426;
		}

		private void DeleteLI()
		{
			LigneIntervention selectedLI = (LigneIntervention)DG_LI.SelectedItem;

			if (selectedLI != null)
			{
				MessageBoxResult dr = MessageBox.Show("Etes-vous sur de vouloir supprimer cette intervention?", "Alerte supression", MessageBoxButton.YesNo);
				if (dr == MessageBoxResult.Yes)
				{
					listLI.Remove(selectedLI);
					
					if(toAddListLI.Any(x => x == selectedLI))
						toAddListLI.Remove(selectedLI);
					else
						dbEntity.LigneIntervention.Remove(selectedLI);
				}
			}
			else
			{
				MessageBox.Show("Merci de sélectionner une ligne d'intervention.");
			}
			ToUpdateLI();
		}

		private void SaveBI()
		{
			BonIntervention bufferBI;

			if (CBB_Personnel.Text != "" && TXT_NomClient.Text != "")
			{
				if (!parentIsModif)
				{
					BonIntervention newBI;
					if (CBB_NumContrat.Text != "" && CBB_NumContrat.Text != "Hors Contrat")
					{
						newBI = new BonIntervention
						{
							Date = dateBI,
							HorsContrat = (Boolean)CBX_OutOfContrat.IsChecked,
							Interlocuteur = CBB_Personnel.Text,
							NumeroBon = LBL_NumBI.Content.ToString(),
							Client = (from client in dbEntity.Client where client.Nom == TXT_NomClient.Text select client).First(),
							Contrat = (from contrat in dbEntity.Contrat where contrat.NumContrat == CBB_NumContrat.Text select contrat).First()
						};

						dbEntity.BonIntervention.Add(newBI);

					}
					else
					{
						newBI = new BonIntervention
						{
							Date = dateBI,
							HorsContrat = (Boolean)CBX_OutOfContrat.IsChecked,
							Interlocuteur = CBB_Personnel.Text,
							NumeroBon = LBL_NumBI.Content.ToString(),
							Client = (from client in dbEntity.Client where client.Nom == TXT_NomClient.Text select client).First(),
							Contrat = null
						};

						dbEntity.BonIntervention.Add(newBI);
					}

					bufferBI = newBI;
				}
				else
				{
					if (oBonSel.Contrat != null)
					{
						oBonSel.Contrat.NumContrat = CBB_NumContrat.Text;
					}

					oBonSel.Client.Nom = TXT_NomClient.Text;
					oBonSel.Date = dateBI;
					oBonSel.HorsContrat = (Boolean)CBX_OutOfContrat.IsChecked;
					oBonSel.Interlocuteur = CBB_Personnel.Text;
					oBonSel.NumeroBon = LBL_NumBI.Content.ToString();

					bufferBI = oBonSel;

				}

				foreach (LigneIntervention li in listLI)
				{
				    li.IdBon = bufferBI.Id;
				}

                var reqContratId = from projettocontrat in dbEntity.ProjetToContrat
                                   join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                   where contrat.NumContrat == CBB_NumContrat.Text
                                   select projettocontrat.idContrat;
                int idContrat = 0;
                foreach (var result in reqContratId)
                {
                    idContrat = (int)result;
                }

                var reqContrat = from projettocontrat in dbEntity.ProjetToContrat
                                 join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                 where contrat.NumContrat == CBB_NumContrat.Text
                                 select contrat.HeuresRestantes;

                double contratHeure = 0;

                foreach(var result in reqContrat)
                {
                    contratHeure = (double) result;
                }

               

				foreach (LigneIntervention li in toAddListLI)
				{
					dbEntity.LigneIntervention.Add(li);
                    contratHeure = contratHeure - li.NbHeures;
                    
                }


                var connectionString = Function.connectDatabase() ;
                SqlConnection con = new SqlConnection(connectionString);
                if ((bool)!CBX_Bug.IsChecked)
                {
                    try
                    {

                        
                        if((contratHeure - UpDown_NombreHeures.Value ) < 0)
                        {
                           if(MessageBox.Show("Le contrat va passé en heure négative si vous effectuez ce bon intervention, êtes-vous sûr ?", "Informations", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes){
                                con.Open();
                                SqlCommand reqUpdateHeure = new SqlCommand();
                                reqUpdateHeure.Connection = con;
                                reqUpdateHeure.CommandText = "UPDATE Contrat SET HeuresRestantes = @nbrHeures WHERE id = @IdContrat";
                                reqUpdateHeure.Parameters.AddWithValue("@nbrHeures", contratHeure);
                                reqUpdateHeure.Parameters.AddWithValue("@IdContrat", idContrat);

                                reqUpdateHeure.ExecuteNonQuery();
                            }
                            
                           
                        }
                        
                        

                    }
                    catch (Exception e)
                    {
                       
                    }
                }
                

                dbEntity.SaveChanges();
				ParentToUpdate();
				wordBonSel = bufferBI;
				isBIValid = true;
			}
			else
			{
				isBIValid = false;
				MessageBox.Show("Merci de Remplir les champs Nom-Client et Intervenant");
			}
		}

		private void ToUpdateLI()
		{
			qli = from li in dbEntity.LigneIntervention select li;
			LiShow();
			DG_LI.ItemsSource = listLI;
			DG_LI.Items.Refresh();
		}

		private void LiShow()
		{
			if (qli.ToList() != null && isLIModif)
			{
				listLI.Clear();
				foreach (LigneIntervention li in qli)
				{
					try
					{
						if (li.IdBon == oBonSel.Id)
						{
							listLI.Add(li);
						}
					}
					catch (Exception)
					{
						listLI.Add(li);
					}
				}
			}
		}

		private void ClearFields()
		{
			TXT_LITitre.Text = "";
			TXT_Description.Text = "";
			CBB_NomProjet.Text = "";
			CBB_Intervenant.SelectedItem = null;
			UpDown_NombreHeures.Value = null;
			Date_DateLI.Text = DateTime.Now.ToString();
		}

		private void FillFieldsLinkToClientNom()
		{
			try
			{

				CBB_NomProjet.ItemsSource = (from projet in dbEntity.Projet
											 where projet.Client.Nom == CBB_NomClient.Text
											 select projet).ToList();

				CBB_NomProjet.SelectedIndex = 0;
				if (CBB_NomProjet.Items.Count == 0)
				{
					CBB_NomProjet.IsEnabled = false;
				}
				else
				{
					CBB_NomProjet.IsEnabled = true;
				}

				CBB_NumContrat.ItemsSource = (from contrat in dbEntity.Contrat
											  join projet in dbEntity.Projet on contrat.Id equals projet.IdContrat
											  join client in dbEntity.Client on projet.IdClient equals client.id
											  where client.Nom == TXT_NomClient.Text
											  group contrat by contrat.NumContrat into g
											  select g).ToList();

				CBB_NumContrat.SelectedIndex = 0;
				if (CBB_NumContrat.Items.Count != 0)
				{
					CBB_NumContrat.IsEnabled = true;
				}
				else
				{
					CBB_NumContrat.IsEnabled = false;
				}
                var req = (from client in dbEntity.Client where client.Nom == CBB_NomClient.Text select client).ToList();
                if (req.Any())
                {
                    Client getDataClient = req.First();
                    TXT_ClientTel.Text = getDataClient.Numero;
                    TXT_ClientVille.Text = getDataClient.Ville;
                }
                

			}
			catch (Exception)
			{
				TXT_ClientTel.Text = "";
				TXT_ClientVille.Text = "";
			}
		}

		private void TXT_NomClient_LostFocus(object sender, RoutedEventArgs e)
		{
			FillFieldsLinkToClientNom();
		}

		private void CBB_NomClient_LostFocus(object sender, RoutedEventArgs e)
		{
			FillFieldsLinkToClientNom();
		}

		private void BTN_OpenClientWindow_Click(object sender, RoutedEventArgs e)
		{
			if (parentIsModif)
			{
				Client editClient = (from client in dbEntity.Client
									join bi in dbEntity.BonIntervention on client.id equals bi.IdClient
									where client.Nom == TXT_NomClient.Text
									select client).First();

				AbstractManageClient manageClient = new AbstractManageClient(editClient, parentIsModif, TXT_NomClient.Text, TXT_ClientVille.Text, TXT_ClientTel.Text, this);
				manageClient.Show();
			}
			else
			{
				AbstractManageClient manageClient = new AbstractManageClient(null, parentIsModif, TXT_NomClient.Text, TXT_ClientVille.Text, TXT_ClientTel.Text, this);
				manageClient.Show();
			}
		}

		public void ParentUpdateClient(String nomClient, String villeClient, String telClient)
		{
			TXT_ClientTel.Text = telClient;
			TXT_NomClient.Text = nomClient;
			TXT_ClientVille.Text = villeClient;
		}

		public class AbstractManageClient : ManageClient
		{
			readonly private ManageBI parentManageBI;

			public AbstractManageClient(Client clientSel, bool isModif, string clientNom, string ville, string tel, ManageBI parent) : base(clientSel, isModif, clientNom, ville, tel)
			{
				InitializeComponent();
				parentManageBI = parent;
			}

			public override void ToUpdateParent()
			{
				parentManageBI.dbEntity.SaveChanges();
				parentManageBI.ParentUpdateClient(TXT_ClientNom.Text, TXT_ClientVille.Text, TXT_ClientTel.Text);
				parentManageBI.BTN_AcceptClient.IsEnabled = false;
			}
		}

		private void BTN_AcceptClient_Click(object sender, RoutedEventArgs e)
		{
			Client clientExists = (from client in dbEntity.Client where client.Nom == TXT_NomClient.Text select client).FirstOrDefault();


            if (clientExists == null)
            {
                if (TXT_ClientTel.Text == "" || TXT_NomClient.Text == "" || TXT_ClientVille.Text == "")
                {
                    MessageBox.Show("Merci de remplir tout les champs précédés d'un '*'");
                }
                dbEntity.Client.Add(new Client
                {
                    Nom = TXT_NomClient.Text,
                    Ville = TXT_ClientVille.Text,
                    Numero = TXT_ClientTel.Text,
                    Adresse = "",
                    CodePostal = ""
                });
            }
            else
            if (clientExists.Nom == TXT_NomClient.Text)
			{
				if (TXT_ClientTel.Text.Count() == 10)
				{
					clientExists.Ville = TXT_ClientVille.Text;
					clientExists.Numero = TXT_ClientTel.Text;
				}
				else
				{
					MessageBox.Show("Merci de rentrer un numéro de téléphone composé de 10 chiffres.");
				}
			}

			dbEntity.SaveChanges();

			BTN_AcceptClient.IsEnabled = false;
		}

		private void TXT_ClientTel_KeyUp(object sender, KeyEventArgs e)
		{
			Client dataClient = (from client in dbEntity.Client where client.Nom == CBB_NomClient.Text select client).First();

			if (TXT_ClientTel.Text != dataClient.Numero)
			{
				BTN_AcceptClient.IsEnabled = true;
			}
			else
			{
				BTN_AcceptClient.IsEnabled = false;
			}
		}

		private void TXT_ClientVille_KeyUp(object sender, KeyEventArgs e)
		{
			Client dataClient = (from client in dbEntity.Client where client.Nom == TXT_NomClient.Text select client).First();

			if (TXT_ClientVille.Text != dataClient.Ville)
			{
				BTN_AcceptClient.IsEnabled = true;
			}
			else
			{
				BTN_AcceptClient.IsEnabled = false;
			}
		}

		private void BTN_EditWord_Click(object sender, RoutedEventArgs e)
		{
			SaveBI();
            MessageBoxResult dr = MessageBox.Show("Voulez-vous envoyé par mail un PDF pour le bon intervention n° "+wordBonSel.NumeroBon + " ?\n\nChemin de sauvegarde: C://ADG-Assistance/Documents/Bon Intervention", "Alerte génération", MessageBoxButton.YesNo, MessageBoxImage.Information);

            if (dr == MessageBoxResult.Yes)
            {
                EditWord doc = new EditWord(LBL_NumBI.Content.ToString(), wordBonSel);
            }
            
		}
	}
}
