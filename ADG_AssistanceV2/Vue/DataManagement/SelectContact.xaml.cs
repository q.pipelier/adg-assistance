﻿using ADG_Assistance.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
    /// <summary>
    /// Logique d'interaction pour SelectContact.xaml
    /// </summary>
    public partial class SelectContact : Window
    {
        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        private ManageProjet manageProjet;
        public Boolean ToModif
        {
            get; set;
        }

        public SelectContact(ManageProjet projet)
        {
            ToModif = false;
            InitializeComponent();
            manageProjet = projet;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            fillDataGrid();
        }

        private void BTN_AddContact_Click(object sender, RoutedEventArgs e)
        {
            ToModif = false;
            Contact selectedContact = (Contact)contactDataGrid.SelectedItem;
            ManageContact manageContact = new ManageContact(this, selectedContact, dbEntity, false, null);
            manageContact.Show();
        }

        private void BTN_ModifyContact_Click(object sender, RoutedEventArgs e)
        {
            ToModif = true;
            Contact selectedContact = (Contact)contactDataGrid.SelectedItem;
            ManageContact manageContact = new ManageContact(this, selectedContact, dbEntity, false, null);
            manageContact.Show();
        }

        private void BTN_Accept_Click(object sender, RoutedEventArgs e)
        {
            selectContact();
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Row_DoubleClick(Object sender, MouseButtonEventArgs e)
        {
            selectContact();
        }

        private void selectContact()
        {
            Boolean addContact = true;
            var selectContact = contactDataGrid.SelectedItem;

            if (selectContact != null)
            {
                var projetContact = dbEntity.Set<ContactProjet>();

                foreach (ContactProjet testContact in dbEntity.ContactProjet.ToList<ContactProjet>())
                {
                    if (testContact.IdContact.ToString() == ((Contact)contactDataGrid.SelectedItem).Id.ToString() &&
                        (testContact.IdProjet.ToString() == (manageProjet.selectedProjet).Id.ToString()))
                    {

                        addContact = false;
                        break;

                    }
                }

                if (addContact)
                {
                    projetContact.Add(new ContactProjet
                    {
                        IdContact = ((Contact)contactDataGrid.SelectedItem).Id,
                        IdProjet = (manageProjet.selectedProjet).Id
                    });
                    dbEntity.SaveChanges();
                    this.manageProjet.ToUpdate();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ce contact est déja dans la liste des contacts du projet.\nMerci de sélectionner un contact qui n'est pas dans la liste des contacts du projet.");
                }
            }
            else
            {
                MessageBox.Show("Merci de bien vouloir sélectionner un contact.");
            }
        }

        internal void toUpdate()
        {
            contactDataGrid.Items.Refresh();
            fillDataGrid();
        }

        public void fillDataGrid()
        {
            CollectionViewSource contactViewSource = ((CollectionViewSource)(this.FindResource("contactViewSource")));

            dbEntity.Contact.Load();
            contactViewSource.Source = dbEntity.Contact.Local;
        }
    }
}
