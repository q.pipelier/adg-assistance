﻿using ADG_Assistance.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
    /// <summary>
    /// Logique d'interaction pour ManageAssistance.xaml
    /// </summary>
    public partial class ManageAssistance : Window
    {

        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        private ManageProjet manageProjet;
        private Assistance manageAssistance;
        private bool isModif;
        public ManageAssistance(ManageProjet projet, bool modif)
        {

            InitializeComponent();
            manageProjet = projet;
            isModif = modif;
        }

        public ManageAssistance(ManageProjet projet, Assistance assistance, bool modif)
        {

            InitializeComponent();
            manageProjet = projet;
            isModif = modif;
            manageAssistance = assistance;

            FillFields();
        }

        private void BTN_CancelAssistance_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BTN_AddAssistance_Click(object sender, RoutedEventArgs e)
        {

            

            if (Nom_TXT.Text == "")
            {
                MessageBox.Show("Merci de remplir le champ nom qui est obligatoire");
            }
            else
            {
                if (!isModif)
                {
                    var assistance = dbEntity.Set<Assistance>();
                    var Projet_req = from projet in dbEntity.Projet
                                     select projet.Id;

                    int last_projet = 0;

                    foreach (var last in Projet_req)
                    {
                        if (last > last_projet)
                        {
                            last_projet = last;
                        }
                    }
                    last_projet = last_projet;

                    if (isModif == true)
                    {
                        assistance.Add(new Assistance
                        {
                            nom = Nom_TXT.Text,
                            id_de_connexion = idConnexion_TXT.Text,
                            mot_de_passe = MotDePasseTV_TXT.Text,
                            serveur = Serveur_TXT.Text,
                            utilisateur_session = Utilisateur_TXT.Text,
                            mot_de_passe_session = MotDePasse_Session_TXT.Text,
                            idProjet = (manageProjet.selectedProjet).Id
                        });
                    }
                    else
                    {
                        assistance.Add(new Assistance
                        {
                            nom = Nom_TXT.Text,
                            id_de_connexion = idConnexion_TXT.Text,
                            mot_de_passe = MotDePasseTV_TXT.Text,
                            serveur = Serveur_TXT.Text,
                            utilisateur_session = Utilisateur_TXT.Text,
                            mot_de_passe_session = MotDePasse_Session_TXT.Text,
                            idProjet = last_projet
                        });
                    }
                }
                else
                {
                    manageAssistance.nom = Nom_TXT.Text;
                    manageAssistance.serveur = Serveur_TXT.Text;
                    manageAssistance.utilisateur_session = Utilisateur_TXT.Text ;
                    manageAssistance.mot_de_passe_session = MotDePasse_Session_TXT.Text ;
                    manageAssistance.mot_de_passe = MotDePasseTV_TXT.Text;
                    manageAssistance.id_de_connexion = idConnexion_TXT.Text;
                }
                

         
                this.Close();
                
            }
            dbEntity.SaveChanges();
            manageProjet.ToUpdate();

            
        }

        private void FillFields()
        {
            Nom_TXT.Text = manageAssistance.nom;
            Serveur_TXT.Text = manageAssistance.serveur;
            Utilisateur_TXT.Text = manageAssistance.utilisateur_session;
            MotDePasse_Session_TXT.Text = manageAssistance.mot_de_passe_session;
            MotDePasseTV_TXT.Text = manageAssistance.mot_de_passe;
            idConnexion_TXT.Text = manageAssistance.id_de_connexion;
        }
    }
}
