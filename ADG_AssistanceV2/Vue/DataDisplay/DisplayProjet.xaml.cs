﻿using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ADG_Assistance.Vue.DataDisplay
{
	/// <summary>
	/// Logique d'interaction pour DisplayProjet.xaml
	/// </summary>
	public partial class DisplayProjet : Page
	{
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		public Projet selectedProjet
        {
            get;
            set;
        }
		private IQueryable<Projet> qProjet;
		MainWindow m_oWin;

		public DisplayProjet(MainWindow oWin)
		{
			InitializeComponent();
			m_oWin = oWin;
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			ToUpdate();
			m_oWin.Title = this.Title;
		}

		private void BTN_AddProjet_Click(object sender, RoutedEventArgs e)
		{
            try
            {

                Projet oNewProjet = new Projet();
                oNewProjet.intialise();
                dbEntity.Projet.Add(oNewProjet);
                dbEntity.SaveChanges();

                ManageProjet newProjet = new ManageProjet(oNewProjet, false, dbEntity, m_oWin, this);
                newProjet.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue lors de l'initialisation du projet : " + ex.Message);
            }

        }

		private void BTN_ModifyProjet_Click(object sender, RoutedEventArgs e)
		{
			ModifyProjet();
		}

		private void ModifyProjet()
		{
			if (DG_Projet.SelectedItem != null)
			{
				selectedProjet = DG_Projet.SelectedItem as Projet;
				ManageProjet modifProjet = new ManageProjet(selectedProjet, true, dbEntity, m_oWin, this);
				modifProjet.Show();
			}
			else
			{
				MessageBox.Show("Merci de sélectionner une ligne du tableau à modifier");
			}
		}

		private void BTN_DeleteProjet_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (DG_Projet.SelectedItem == null)
				{
					MessageBox.Show("Veuillez selectionner un projet");
				}
				else
				{
					Projet projet = DG_Projet.SelectedItem as Projet;
					MessageBoxResult dr = MessageBox.Show("Etes-vous sûr de vouloir supprimer le projet " + projet.Nom + " ?", "Alerte supression", MessageBoxButton.YesNo);

					if (dr == MessageBoxResult.Yes)
					{
                        //--- On vérifie que le projet n'est pas ligne à des bons d'interventions ---//
						if ( !projet.LigneIntervention.Any() )
						{

                            dbEntity.Assistance.RemoveRange(projet.Assistance);
                            dbEntity.ContactProjet.RemoveRange(projet.ContactProjet);
							dbEntity.Projet.Remove(projet);
							dbEntity.SaveChanges();
							ToUpdate();
						}
						else
						{
							MessageBox.Show("Impossible de supprimer ce projet, il est lié à un ou plusieurs interventions.");
						}
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Une erreur est survenue lors de la suppression du projet : " + ex.Message);
			}
		}
        ListSortDirection sortDirection;
        public void ToUpdate()
		{
			qProjet = from projet in dbEntity.Projet select projet;
			DG_Projet.ItemsSource = qProjet.ToList();
			DG_Projet.Items.Refresh();
            applySortDescriptions(sortDirection);
		}

		private void BTN_ClearFilters_Click(object sender, RoutedEventArgs e)
		{
			TXT_ClientNom.Text = "";
			TXT_ProjetNom.Text = "";
		}

		private void BTN_Research_Click(object sender, RoutedEventArgs e)
		{
			ToUpdate();
		}

		private void DG_ProjetRow_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			ModifyProjet();
		}

		private void CheckEnterPressed(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}
        int selectedColumnIndex;

        private void DG_Projet_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = (e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);

        }

        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            DG_Projet.Items.SortDescriptions.Clear();
            string propertyName = DG_Projet.Columns[selectedColumnIndex].SortMemberPath;
            DG_Projet.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));
            applySortDirection(listSortDirection);
            DG_Projet.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_Projet.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }
    }
}