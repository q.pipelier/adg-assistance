﻿using ADG_Assistance.DB;
using ADG_Assistance.Functions;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Caching;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ADG_Assistance.Vue.DataDisplay
{
    /// <summary>
    /// Logique d'interaction pour DisplayContrat.xaml
    /// </summary>
    public partial class DisplayContrat : Page
    {

        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        Contrat oContratSel;
        
        Projet oProjetSel;
        public Boolean isModif { get; set; }
        private IQueryable<Projet> qProjet;
        
        private List<NomClient> clients = new List<NomClient>();
        private IQueryable<Contrat> qContrat;
        private List<Contrat> listContratFiltered = new List<Contrat>();
        MainWindow m_oWin;
        System.Runtime.Caching.ObjectCache cache = MemoryCache.Default;

        public DisplayContrat(MainWindow oWin)
        {
            isModif = false;
            InitializeComponent();
            displayProjet();
            m_oWin = oWin;

            object roleConvert = cache.Get("Role");
            string role = roleConvert.ToString();
            if (role != "Administrateur")
            {
                this.BTN_AddContrat.Visibility = Visibility.Hidden;
                this.BTN_AddContratProjet.Visibility = Visibility.Hidden;
                this.BTN_DeleteContrat.Visibility = Visibility.Hidden;
                this.BTN_DeleteContratProjet.Visibility = Visibility.Hidden;
                
                this.BTN_ModifyContratProjet.Visibility = Visibility.Hidden;
                this.BTN_RemoveContratProjet.Visibility = Visibility.Hidden;
                this.BTN_DeleteContrat.Visibility = Visibility.Hidden;

                this.BTN_CheckContrat.Visibility = Visibility.Visible;
                this.BTN_ModifyContrat.Visibility = Visibility.Hidden;
            }

        }

        public void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ToUpdate();
            m_oWin.Title = this.Title;
        }

        private void ProjetContratDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedItem();
        }

        private void displayProjet()
        {
            if (oContratSel != null)
            {
                qProjet = from projet in dbEntity.Projet
                          join contrat in dbEntity.Contrat on projet.IdContrat equals contrat.Id
                          where contrat.Id == oContratSel.Id
                          //Pas eu le temps de finir ce filtre
                          //where projet.Nom.ToUpper().Contains(TXT_FilterNomProjet.Text.ToUpper())
                          orderby projet.Nom
                          select projet;

                DG_ContratProjet.ItemsSource = qProjet.ToList();
            }
        }

        private void selectedItem()
        {
            
            NomClient nomClient = (NomClient) DG_Contrat.SelectedItem;

            try
            {
                oContratSel = new Contrat()
                {
                    NumContrat = nomClient.NumContrat,
                    DateDeb = nomClient.DateDeb,
                    DateFin = nomClient.DateFin,
                    Id = nomClient.contratId,
                    HeuresRestantes = nomClient.HeuresRestantes,
                    NbHeures = nomClient.NbHeures,
                    DateSignature = nomClient.DateSignature,
                    Actif = nomClient.Actif
                };
            }
            catch { 
            }
            
            

            displayProjet();
        }

        private void BTN_AddContrat_Click(object sender, RoutedEventArgs e)
        {
            isModif = false;
            ManageContrat manageContrat = new ManageContrat(this, dbEntity, m_oWin);
            manageContrat.Show();
            ToUpdate();
        }

        private void BTN_ModifyContrat_Click(object sender, RoutedEventArgs e)
        {
            modifyContrat();
        }

        private void BTN_DeleteContrat_Click(object sender, RoutedEventArgs e)
        {
            String messageboxText;
            if (oContratSel != null)
            {
                try
                {

                    NomClient nomClient = (NomClient)DG_Contrat.SelectedItem;
                    var reqIdProjet = from projettocontrat in dbEntity.ProjetToContrat
                                      where projettocontrat.idContrat == nomClient.contratId
                                      select projettocontrat.idProjet;

                    int idProj = 0;

                    foreach(var project in reqIdProjet)
                    {
                        idProj = (int) project;
                    }

                    var reqId = from projettocontrat in dbEntity.ProjetToContrat
                                      where projettocontrat.idContrat == nomClient.contratId
                                      where projettocontrat.idProjet == idProj
                                      select projettocontrat.id;

                    int idToProjetContrat = 0;

                    foreach (var projecttocontrat in reqId)
                    {
                        idToProjetContrat = projecttocontrat;
                    }

                    var connectionString = Function.connectDatabase();
                    messageboxText = "Etes-vous sûr de vouloir supprimer le contrat " + nomClient.NumContrat + " ?";
                    MessageBoxResult dr = MessageBox.Show(messageboxText, "Alerte supression", MessageBoxButton.YesNo);
                    if (dr == MessageBoxResult.Yes)
                    {
                        
                        SqlConnection con = new SqlConnection(connectionString);
                        try
                        {
                            con.Open();
                            SqlCommand reqUpdateHeure = new SqlCommand();
                            reqUpdateHeure.Connection = con;
                            reqUpdateHeure.CommandText = "DELETE FROM ProjetToContrat WHERE id=@id";
                            reqUpdateHeure.Parameters.AddWithValue("@id", idToProjetContrat);


                            reqUpdateHeure.ExecuteNonQuery();

                        }
                        catch
                        {
                            
                        }

                        DG_Contrat.Items.Refresh();

                        
                        ToUpdate();
                    }
                }
                catch (Exception ex)
                {
                    
                }
            }
            else
            {
                MessageBox.Show("Merci de sélectionner un Contrat");
            }
        }
        ListSortDirection sortDirection;
        public void ToUpdate()
        {
            qContrat = from contrat in dbEntity.Contrat select contrat;
            Filters();

            var contratFin = from projettocontrat in dbEntity.ProjetToContrat
                             join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                             select contrat.DateFin;

            var contratId = from projettocontrat in dbEntity.ProjetToContrat
                            join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                            select contrat.Id;

            List<int> idContratExist = new List<int>();

            foreach(var verif in contratId)
            {
                idContratExist.Add(verif);
                
            }
            var connectionString = Function.connectDatabase();
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            
            int idV = 0;
            foreach (var verif in contratFin)
            {
                int resultDateCompare = DateTime.Compare((DateTime) verif, DateTime.Today);
                if(resultDateCompare < 0)
                {
                    
                    try
                    {
                       
                        SqlCommand reqUpdateHeure = new SqlCommand();
                        reqUpdateHeure.Connection = con;
                        reqUpdateHeure.CommandText = "UPDATE Contrat SET Actif = @actif WHERE Id = @id";
                        reqUpdateHeure.Parameters.AddWithValue("@id", idContratExist[idV]);
                        reqUpdateHeure.Parameters.AddWithValue("@actif", 0);
                        reqUpdateHeure.ExecuteNonQuery();

                    }
                    catch
                    {

                    }
                }
                idV = idV + 1;
            }

            toUpdateDG();
            
            DG_Contrat.ItemsSource = clients;
            DG_Contrat.Items.Refresh();

            applySortDescriptions(sortDirection);
        }

        private void Filters()
        {
            clients.Clear();

            // TODO


            if (qContrat.ToList() != null)
            {
                foreach (Contrat contrat in qContrat)
                {
                    if (contrat.NumContrat.ToUpper().Contains(TXT_FilterNumContrat.Text.ToUpper()) &&
                        (contrat.designation.ToUpper().Contains(TXT_FilterNomClient.Text.ToUpper())))
                    {

                        Boolean findProjet = false;

                        foreach (Projet projet in contrat.Projet)
                        {
                            if (projet.Nom.ToUpper().Contains(TXT_FilterNomProjet.Text.ToUpper()))
                            {
                                findProjet = true;
                            }
                        }
                        if (findProjet)
                        {
                            listContratFiltered.Add(contrat);
                        }

                        if (contrat.Projet.Count == 0)
                        {
                            listContratFiltered.Add(contrat);
                        }
                        
                    }
                }
            }
            displayProjet();
        }

        private void DG_ContratRow_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            modifyContrat();
        }

        private void DG_ContratProjetRow_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ModifyContratProjet();
        }

        private void modifyContrat()
        {
            isModif = true;
            ManageContrat manageContrat = new ManageContrat(this, oContratSel, dbEntity, m_oWin);
            manageContrat.Show();
        }

        private void BTN_AddContratProjet_Click(object sender, RoutedEventArgs e)
        {
            SelectProjet select = new SelectProjet(this, oContratSel, m_oWin, dbEntity, "");
            select.Show();

            if (oProjetSel != null)
            {
                select.oProjetSel.IdContrat = oProjetSel.IdContrat;
                dbEntity.SaveChanges();
                ToUpdate();
            }

        }

        private void BTN_ModifyContratProjet_Click(object sender, RoutedEventArgs e)
        {
            ModifyContratProjet();
        }

        private void BTN_RemoveContratProjet_Click(object sender, RoutedEventArgs e)
        {
            oProjetSel = (Projet)DG_ContratProjet.SelectedItem;

            oProjetSel.IdContrat = null;
            dbEntity.SaveChanges();
            ToUpdate();
        }

        private void BTN_DeleteContratProjet_Click(object sender, RoutedEventArgs e)
        {
            oProjetSel = (Projet)DG_ContratProjet.SelectedItem;
            if (oProjetSel != null)
            {
                IQueryable<ContactProjet> lst = (from contratProjet in dbEntity.ContactProjet where contratProjet.IdProjet == oProjetSel.Id select contratProjet);

                if (!lst.Any())
                {
                    dbEntity.Projet.Remove(oProjetSel);
                    dbEntity.SaveChanges();
                    ToUpdate();
                }
                else
                {
                    MessageBox.Show("Impossible de supprimer ce projet, il est lié à un ou plusieurs contacts, vérifiez la table ContactProjet");
                }
            }
        }

        private void ModifyContratProjet()
        {
            oProjetSel = (Projet)DG_ContratProjet.SelectedItem;

            if (oProjetSel != null)
            {
                ManageProjet manageProjet = new ManageProjet(oProjetSel, true, dbEntity, m_oWin, this);
                manageProjet.Show();
            }
        }

        private void BTN_ClearFilters_Click(object sender, RoutedEventArgs e)
        {
            TXT_FilterNomClient.Text = "";
            TXT_FilterNomProjet.Text = "";
            TXT_FilterNumContrat.Text = "";
        }

        private void BTN_Research_Click(object sender, RoutedEventArgs e)
        {
            ToUpdate();
        }

        private void CheckEnterPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ToUpdate();
            }
        }
        int selectedColumnIndex;
        private void DG_Contrat_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = ListSortDirection.Descending;

        }
        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            DG_Contrat.Items.SortDescriptions.Clear();
            string propertyName = DG_Contrat.Columns[selectedColumnIndex].SortMemberPath;
            DG_Contrat.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));
            applySortDirection(listSortDirection);
            DG_Contrat.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_Contrat.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }

        public void toUpdateDG()
        {

            
            List<String> listNumContrat = new List<String>();
            List<DateTime> listDateCreation = new List<DateTime>();
            List<DateTime> listDateFin = new List<DateTime>();
            List<DateTime> listDateDebut = new List<DateTime>();
            List<int> listNbHeures = new List<int>();
            List<int> listHeuresRestantes = new List<int>();
            List<int> listContratId = new List<int>();
            List<DateTime> listDateSignature = new List<DateTime>();
            List<bool> listActif = new List<bool>();

            var reqIdContrat = from projettocontrat in dbEntity.ProjetToContrat
                               join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                               select contrat.Id;

            var reqNumContrat = from projettocontrat in dbEntity.ProjetToContrat
                                           join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                           select contrat.NumContrat;

            var reqDateCreation = from projettocontrat in dbEntity.ProjetToContrat
                                               join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                               select contrat.DateCreation;

            var reqDateFin = from projettocontrat in dbEntity.ProjetToContrat
                                  join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                                  select contrat.DateFin;

            var reqDateDeb = from projettocontrat in dbEntity.ProjetToContrat
                             join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                             select contrat.DateDeb;

            var reqDateSignature = from projettocontrat in dbEntity.ProjetToContrat
                             join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                             select contrat.DateSignature;

            var reqNbHeures = from projettocontrat in dbEntity.ProjetToContrat
                             join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                             select contrat.NbHeures;

            var reqHeuresRestantes = from projettocontrat in dbEntity.ProjetToContrat
                              join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                              select contrat.HeuresRestantes;

            var reqClient = from projettocontrat in dbEntity.ProjetToContrat
                        join projet in dbEntity.Projet on projettocontrat.idProjet equals projet.Id
                        join client in dbEntity.Client on projet.IdClient equals client.id
                        select client.Nom;

            var reqActif = from projettocontrat in dbEntity.ProjetToContrat
                              join contrat in dbEntity.Contrat on projettocontrat.idContrat equals contrat.Id
                              select contrat.Actif;

            int id = 0;

            foreach (var actif in reqActif)
            {
                
                listActif.Add((bool)actif);
                
            }

            foreach (var idContrat in reqIdContrat)
            {
                listContratId.Add(idContrat);
            }

            foreach (var numContrat in reqNumContrat)
            {
                listNumContrat.Add(numContrat);
            }

            foreach(var dateSigna in reqDateSignature)
            {
                listDateSignature.Add((DateTime)dateSigna);
            }

            foreach (var dateCrea in reqDateCreation)
            {
                listDateCreation.Add((DateTime)dateCrea);
            }

            foreach (var dateFin in reqDateFin)
            {
                listDateFin.Add((DateTime)dateFin);
            }

            foreach (var dateDebut in reqDateDeb)
            {
                listDateDebut.Add((DateTime)dateDebut);
            }

            foreach (var heures in reqNbHeures)
            {
                listNbHeures.Add((int)heures);
            }

            foreach (var heuresRestantes in reqHeuresRestantes)
            {
                listHeuresRestantes.Add((int)heuresRestantes);
            }

            foreach (var nameClient in reqClient)
            {
                
                clients.Add(new NomClient() {
                    NumContrat = listNumContrat[id],
                    Client = nameClient,
                    DateCreation = listDateCreation[id],
                    DateDeb = listDateDebut[id],
                    DateFin = listDateFin[id] ,
                    HeuresRestantes = listHeuresRestantes[id],
                    NbHeures = listNbHeures[id],
                    contratId = listContratId[id],
                    DateSignature = listDateSignature[id],
                    Actif = listActif[id]});
                id = id + 1;
            }

           
        }

    }

    public class NomClient
    {
        public int Id { get; set; }

        public string NumContrat { get; set; }

        public DateTime DateCreation { get; set; }

        public DateTime DateFin { get; set; }

        public DateTime DateDeb { get; set; }

        public DateTime DateSignature { get; set; }

        public int NbHeures { get; set; }

        public int HeuresRestantes { get; set; }

        public string Client { get; set; }

        public int contratId { get; set;  }
        public Boolean Actif { get; set; }
    }
}
