﻿using ADG_Assistance.Functions;
using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using System.Data.SqlClient;

namespace ADG_Assistance.Vue.DataDisplay
{
	/// <summary>
	/// Logique d'interaction pour DisplayBI.xaml
	/// </summary>
	public partial class DisplayBI : Page
	{
		private List<BonIntervention> listBIFiltered = new List<BonIntervention>();
		private IQueryable<BonIntervention> qBI;
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		private BonIntervention oBISel;
		MainWindow m_oWin;
         

        public DisplayBI(MainWindow oWIn)
		{
			InitializeComponent();
			m_oWin = oWIn;
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			ToUpdate();
			m_oWin.Title = this.Title;
		}
        ListSortDirection sortDirection;

        private void ToUpdate()
		{
			qBI = from bi in dbEntity.BonIntervention select bi;
			Filters();
			DG_BI.ItemsSource = listBIFiltered;
			DG_BI.Items.Refresh();
            applySortDescriptions(sortDirection);
		}

		private void Filters()
		{
			listBIFiltered.Clear();
			if (qBI.ToList() != null)
			{
				foreach (BonIntervention bi in qBI)
				{
					if (bi.Client.Nom == null || bi.Client.Nom.ToUpper().Contains(TXT_NomClient.Text.ToUpper()))
					{
						if (bi.NumeroBon == null || bi.NumeroBon.ToUpper().Contains(TXT_NumBI.Text.ToUpper()))
						{
							if (bi.Contrat == null || bi.Contrat.NumContrat.ToUpper().Contains(TXT_NumContrat.Text.ToUpper()))
							{
								if (CBX_DateEnable.IsChecked == true)
								{
									if (bi.Date == Function.stringToDate(Date_Date.Text))
									{
										listBIFiltered.Add(bi);
									}
								}
								else
								{
									listBIFiltered.Add(bi);
								}
							}
						}
					}
				}
			}
		}

		public void DG_BIRow_DoubleClick(Object sender, MouseButtonEventArgs e)
		{
			ModifyBI();
		}

		private void BTN_AddBI_Click(object sender, RoutedEventArgs e)
		{
			oBISel = (BonIntervention)DG_BI.SelectedItem;
			AbstractManageBI manageBI = new AbstractManageBI(this, oBISel, false, dbEntity);
			manageBI.Show();
		}

		private void BTN_ModifyBI_Click(object sender, RoutedEventArgs e)
		{
			ModifyBI();
		}

		private void BTN_DeleteBI_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				oBISel = (BonIntervention)DG_BI.SelectedItem;
                if (oBISel != null)
                {

                    var reqLI = from ligneintervention in dbEntity.LigneIntervention
                                join bonintervention in dbEntity.BonIntervention on ligneintervention.IdBon equals bonintervention.Id
                                where bonintervention.NumeroBon == oBISel.NumeroBon
                                select ligneintervention.id;

                    int idLI = 0;

                    foreach(var result in reqLI)
                    {
                        idLI = result;
                    }


                   
                    var connectionString = Function.connectDatabase();
                    MessageBoxResult dr = MessageBox.Show("Etes-vous sûr de vouloir supprimer le bon d'intervention " + oBISel.NumeroBon + " ?", "Alerte supression", MessageBoxButton.YesNo);
                    if (dr == MessageBoxResult.Yes)
                    {
                        SqlConnection con = new SqlConnection(connectionString);
                        try
                        {
                            con.Open();
                            SqlCommand reqDeleteLI = new SqlCommand();
                            reqDeleteLI.Connection = con;
                            reqDeleteLI.CommandText = "DELETE FROM LigneIntervention WHERE IdBon=@id";
                            reqDeleteLI.Parameters.AddWithValue("@id", oBISel.Id);


                            reqDeleteLI.ExecuteNonQuery();

                            SqlCommand reqDeleteBI = new SqlCommand();
                            reqDeleteBI.Connection = con;
                            reqDeleteBI.CommandText = "DELETE FROM BonIntervention WHERE Id=@id";
                            reqDeleteBI.Parameters.AddWithValue("@id", oBISel.Id);


                            reqDeleteBI.ExecuteNonQuery();

                        }
                        catch
                        {

                        }
                        ToUpdate();					
                    }
                }
                else
                {
                    MessageBox.Show("Merci de sélectionner un Bon d'Intervention");
                }

            }
			catch (Exception ex)
			{
				MessageBox.Show("Impossible de supprimer le bon d'intervention ex: "+ex.Message);
			}
		}

		private void BTN_ClearFilters_Click(object sender, RoutedEventArgs e)
		{
			CBX_DateEnable.IsChecked = false;
			TXT_NomClient.Text = "";
			TXT_NumBI.Text = "";
			TXT_NumContrat.Text = "";
		}

		private void BTN_Research_Click(object sender, RoutedEventArgs e)
		{
			ToUpdate();
		}

		private void CBX_DateEnable_Checked(object sender, RoutedEventArgs e)
		{
			Date_Date.IsEnabled = true;
			Date_Date.Text = DateTime.Now.ToString();
		}

		private void CBX_DateEnable_Unchecked(object sender, RoutedEventArgs e)
		{
			Date_Date.IsEnabled = false;
			Date_Date.Text = "";
		}

		private void ModifyBI()
		{
			oBISel = (BonIntervention)DG_BI.SelectedItem;
			if (oBISel != null)
			{
				AbstractManageBI manageBI = new AbstractManageBI(this, oBISel, true, dbEntity);
				manageBI.Show();
			}
			else
			{
				MessageBox.Show("Merci de sélectionner un Bon d'Intervention");
			}
			
		}

		public class AbstractManageBI : ManageBI
		{
			readonly private DisplayBI parentDisplayBI;

			public AbstractManageBI(DisplayBI parent, BonIntervention bi, bool isModif, ADG_AssistanceEntities _dbEntity) : base(bi, isModif, _dbEntity)
			{
				InitializeComponent();
				parentDisplayBI = parent;
			}

			public override void ParentToUpdate()
			{
				parentDisplayBI.ToUpdate();
			}
		}

		private void CheckEnterPressed(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}
        int selectedColumnIndex;
        private void DG_BI_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = (e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);

        }

        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            //Clear current sort descriptions 
            DG_BI.Items.SortDescriptions.Clear();

            //Get property name to apply sort based on desired column 
            string propertyName = DG_BI.Columns[selectedColumnIndex].SortMemberPath;

            //Add the new sort description 
            DG_BI.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));

            //apply sort 
            applySortDirection(listSortDirection);

            //refresh items to display sort 
            DG_BI.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_BI.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }
    }
}
