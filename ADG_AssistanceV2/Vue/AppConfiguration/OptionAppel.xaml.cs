﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Reflection;
using ADG_Assistance.DB;
using System.Data;

namespace ADG_Assistance.Vue.AppConfiguration
{
	/// <summary>
	/// Logique d'interaction pour OptionAppel.xaml
	/// </summary>
	public partial class OptionAppel : Page
	{
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();		

		public OptionAppel()
		{
			InitializeComponent();

			CMB_ColorPrioriteFaible.ItemsSource = typeof(Colors).GetProperties();
			CMB_ColorPrioriteImportante.ItemsSource = typeof(Colors).GetProperties();
			CMB_ColorPrioriteMoyenne.ItemsSource = typeof(Colors).GetProperties();
			CMB_ColorTraite.ItemsSource = typeof(Colors).GetProperties();

			var colorFaible = dbEntity.ColorAppel.Find(1);
			var colorMoyenne = dbEntity.ColorAppel.Find(2);
			var colorImportante = dbEntity.ColorAppel.Find(3);
			var colorTraite = dbEntity.ColorAppel.Find(4);


			CMB_ColorPrioriteFaible.Text = "System.Windows.Media.Color " + colorFaible.Couleur;
			CMB_ColorPrioriteImportante.Text = "System.Windows.Media.Color " + colorImportante.Couleur;
			CMB_ColorPrioriteMoyenne.Text = "System.Windows.Media.Color " + colorMoyenne.Couleur;
			CMB_ColorTraite.Text = "System.Windows.Media.Color " + colorTraite.Couleur;
		}

		private void BTN_CancelConfiguration_Click(object sender, RoutedEventArgs e)
		{ 
			CancelColorAppel();
		}

		private void BTN_OkConfiguration_Click(object sender, RoutedEventArgs e)
		{
			SaveColorAppel();
		}

		private void SaveColorAppel()
		{
			var colorFaible = dbEntity.ColorAppel.Find(1);
			var colorMoyenne = dbEntity.ColorAppel.Find(2);
			var colorImportante = dbEntity.ColorAppel.Find(3);
			var colorTraite = dbEntity.ColorAppel.Find(4);

			colorFaible.Couleur = CMB_ColorPrioriteFaible.SelectedItem.ToString().Split(' ').Last();
			colorMoyenne.Couleur = CMB_ColorPrioriteMoyenne.SelectedItem.ToString().Split(' ').Last();
			colorImportante.Couleur = CMB_ColorPrioriteImportante.SelectedItem.ToString().Split(' ').Last();
			colorTraite.Couleur = CMB_ColorTraite.SelectedItem.ToString().Split(' ').Last();

			dbEntity.SaveChanges();

			MessageBox.Show("Couleurs enregistrés : \n\n     - Priorité Faible : " + CMB_ColorPrioriteFaible.SelectedItem.ToString().Split(' ').Last()
				+ "\n     - Priorité Moyenne : " + CMB_ColorPrioriteMoyenne.SelectedItem.ToString().Split(' ').Last()
				+ "\n     - Priorité Importante : " + CMB_ColorPrioriteImportante.SelectedItem.ToString().Split(' ').Last() 
				+ "\n     - Appel traité : " + CMB_ColorTraite.SelectedItem.ToString().Split(' ').Last());
		}

		private void CancelColorAppel()
		{
			CMB_ColorPrioriteFaible.Text = "System.Windows.Media.Color Yellow";
			CMB_ColorPrioriteImportante.Text = "System.Windows.Media.Color Red";
			CMB_ColorPrioriteMoyenne.Text = "System.Windows.Media.Color Orange";
			CMB_ColorTraite.Text = "System.Windows.Media.Color Green";
			SaveColorAppel();
			dbEntity.SaveChanges();
		}
	}
}