﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADG_Assistance.DB
{
	public partial class Contact
	{
		public string fullName
		{
			get
			{
				try
				{
					return this.Nom + " " + this.Prenom;
				}
				catch (Exception)
				{
					return "- - -";
				}
			}

		}
	}
}
