//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ADG_Assistance.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Parametre
    {
        public int id { get; set; }
        public string email { get; set; }
        public string smtp { get; set; }
        public Nullable<int> port { get; set; }
        public string password { get; set; }
        public string LienRedMine { get; set; }
    }
}
