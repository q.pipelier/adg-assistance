﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADG_Assistance.DB
{
    public partial class Projet
    {

        const string URL_DEMANDES_REDMINE = "/issues";

        public string UrlRedmine
        {
            get; set;
        }

        public String UrlProjetRedmine
        {
            get
            {

                return UrlRedmine + this.NomProjetRedmine + URL_DEMANDES_REDMINE;
            }
        }

        public void intialise()
        {
            
            Nom = String.Empty;
        }


    }
}
