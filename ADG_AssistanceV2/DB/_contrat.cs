﻿using ADG_Assistance.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADG_Assistance.DB
{
    public partial class Contrat
    {
        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        public string designation
        {
            get
            {

                try
                {

                    var req = from projettocontrat in dbEntity.ProjetToContrat
                              join projet in dbEntity.Projet on projettocontrat.idProjet equals projet.Id
                              join client in dbEntity.Client on projet.IdClient equals client.id
                              select client.Nom;
                    string test = "";

                    foreach (var result in req)
                    {
                        test = result;

                    }
                    return test;

                }
                catch (Exception)
                {
                    return "- - -";
                }
            }
        }

        public Client client
        {
            get
            {
                try
                {
                    return this.Projet.First().Client;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}
