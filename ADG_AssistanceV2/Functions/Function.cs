﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ADG_Assistance.Functions
{
	public static class Function
	{

		/// <summary>
		/// Convert string into DateTime => dd/MM/yyyy
		/// </summary>
		/// <param name="dateString">string to convert into DateTime</param>
		/// <returns></returns>
		public static DateTime? stringToDate(string dateString)
		{
			try
			{
				return DateTime.ParseExact(dateString, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
			}
			catch (Exception)
			{
				return null;
			}		
		}

        /// <summary>
        /// Déclenche l'évenement Loaded sur la Page
        /// </summary>
        /// <param name="oParent">Page déclenchant l'évènement</param>
        /// <param name="obj">origine</param>
        public static void declencheEvenementLoaded(UIElement oParent, object obj)
        {
            if (oParent != null)
            {
                RoutedEventArgs oEvent = new RoutedEventArgs();
                oEvent.RoutedEvent = Page.LoadedEvent;
                oEvent.Source = obj;
                oParent.RaiseEvent(oEvent);
            }
        }

        public static void AfficheException(Exception ex, string szEnteteMessage, string szEnteteDialog, ILogger Log)
        {
            string szMessage = szEnteteMessage + donneMessageException(ex) + "\n" + ex.StackTrace;

            MessageBox.Show(szMessage, szEnteteDialog, MessageBoxButton.OK, MessageBoxImage.Error);
            Log.Error(szMessage);
        }

        public static void AfficheException(Exception ex, string szEnteteMessage, ILogger Log)
        {
            string szMessage = szEnteteMessage + donneMessageException(ex);
            szMessage += "\n" + ex.StackTrace;
            Log.Error(szMessage);
        }

        public static string donneMessageException(Exception dbEx)
        {
            string szMsgException = "";

            Exception ex = dbEx;

            while (ex != null)
            {
                szMsgException = ex.Message;
                ex = ex.InnerException;
            }



            return szMsgException;
        }

        public static string connectDatabase()
        {
            return "data source=ADGDAGOBAH;initial catalog=ADG_Assistance_test;;user id=ad_user;password=Adg72";
        }
    }
}
