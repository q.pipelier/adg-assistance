﻿using System;
using Word = Microsoft.Office.Interop.Word;
using System.Windows;
using Humanizer;
using static Microsoft.WindowsAPICodePack.Shell.PropertySystem.SystemProperties.System;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using ADG_Assistance.DB;
using System.Linq;
using Microsoft.Office.Interop.Word;
using System.Net.Mail;
using MailMessage = System.Net.Mail.MailMessage;
using System.Net;
using System.Runtime.Caching;
using System.Net.Mime;
using System.Reflection;

namespace ADG_Assistance.Functions
{
    /// <summary>
    /// 
    /// </summary>
    class EditWord
    {
		readonly ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        object missing = System.Reflection.Missing.Value;
        readonly Word.Application msWord;

        #region "constructeurs"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <param name="oCo"></param>
        public EditWord(string num, object selectedObject)
        {
            try
            {

                // connexion à Word
                msWord = new Word.Application();
                msWord.Visible = false;

                if (num.Substring(0, 2) == "BI")
                {
                    CreationBI(num, (BonIntervention)selectedObject);
                }
                else if (num.Substring(0, 2) == "CM")
                {
                    CreationCM(num, (Contrat)selectedObject);
                }
                msWord = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
            }
            finally
            {
                if(msWord != null)
                    msWord.Quit();
                msWord = null;
            }

        }
        #endregion

        private void CreationCM(string num, Contrat contrat)
        {
            try
            {
				int id = contrat.Id;
                if (id == 0)
                {
                    MessageBox.Show("Ce contrat n'existe pas.");
                }
                else
                {
                    Word.Document nvDoc;

					String path = (from word in dbEntity.ModelWord select word.ModelCM).First();

					// Créer le document
					nvDoc = msWord.Documents.Add(path);

                    ReplaceHeader("<NumContrat>", num, nvDoc);
                    ReplaceHeader("<DateDeb>", contrat.DateDeb.Value.ToShortDateString(), nvDoc);
                    ReplaceHeader("<NomEntreprise>", contrat.designation, nvDoc);


                    Replace("<Contact>",contrat.ContactCivilite + " " + contrat.Contact);
                    Replace("<NumContrat>", contrat.NumContrat);
                    Replace("<NomEntreprise>", contrat.designation);
                    Replace("<Adresse>", contrat.client.Adresse);

					if (contrat.client.Complement == "" || contrat.client.Complement == null)
					{
						msWord.Selection.GoTo(Word.WdGoToItem.wdGoToLine, Word.WdGoToDirection.wdGoToFirst, 1, "");
                        msWord.Selection.Find.Execute("<Complement>");
                        msWord.Selection.Delete();
                    }
                    else
                    {
                        Replace("<Complement>", contrat.client.Complement);
                    }
                    Replace("<CodePostal>", contrat.client.CodePostal);
                    Replace("<Ville>", contrat.client.Ville);
                    Replace("<Numero>", contrat.client.Numero);
                    Replace("<DateDeb>", contrat.DateDeb.Value.ToShortDateString());
                    Replace("<NbHeures>", Int32.Parse(contrat.NbHeures.ToString()).ToWords() + " (" + contrat.NbHeures.ToString() + ")");
                    bool pBase = false;

					Projet lastProjet = contrat.Projet.Last();
					foreach (Projet projet in contrat.Projet)
					{
						if (!projet.Equals(lastProjet))
						{
							Replace("<NomProjet>", "solution logicielle sur mesure nommée " + projet.Nom);
							msWord.Selection.MoveDown(Word.WdUnits.wdLine, 1, Word.WdMovementType.wdMove);
							msWord.Selection.MoveLeft(Word.WdUnits.wdCharacter, 1, Word.WdMovementType.wdMove);
							msWord.Selection.InsertParagraphAfter();
							msWord.Selection.InsertAfter("<NomProjet>");
							msWord.Selection.Font.Name = "Times New Roman";

							if (projet.PresenceBase)
							{
								pBase = true;
								Replace("<Base>", "Base de données du logiciel " + projet.Nom);
								msWord.Selection.MoveDown(Word.WdUnits.wdLine, 1, Word.WdMovementType.wdMove);
								msWord.Selection.MoveLeft(Word.WdUnits.wdCharacter, 1, Word.WdMovementType.wdMove);
								msWord.Selection.InsertParagraphAfter();
								msWord.Selection.InsertAfter("<Base>");
								msWord.Selection.Font.Name = "Times New Roman";
								msWord.Selection.Font.Italic = 0;
							}
						}
						else
						{
							Replace("<NomProjet>", "Solution logicielle sur mesure nommée " + projet.Nom);

							if (projet.PresenceBase)
							{
								Replace("<Base>", "Base de données du logiciel " + projet.Nom);
							}

							else
							{
								if (pBase)
								{
									msWord.Selection.GoTo(Word.WdGoToItem.wdGoToLine, Word.WdGoToDirection.wdGoToFirst, 1, "");
									msWord.Selection.Find.Execute("<Base>");
									msWord.Selection.Delete();
									msWord.Selection.Delete(Word.WdUnits.wdCharacter, -1);
								}
							}
						}
					}

                    if (!pBase)
                    {
                        Replace("<Base>", "Les logiciels contractuels ne possèdent pas de bases de données propres");
                    }

                    Replace("<Ville+CP>", contrat.client.Ville + " (" + contrat.client.CodePostal + ")");
                    Replace("<CoutHT>", contrat.Cout.ToString());
                    float coutHT = float.Parse(contrat.Cout.ToString());
                    float coutTTC = coutHT * 1.2f;
                    Replace("<CoutTTC>", coutTTC.ToString());
                    Replace("<DateSignature>", contrat.DateSignature.Value.ToShortDateString());

                    // Attribuer le nom
                    string dateSaisie = contrat.DateCreation.Value.ToShortDateString();
                    string date = dateSaisie.Substring(6) + dateSaisie.Substring(3, 2) + dateSaisie.Substring(0, 2);
					object fileName;
					
					fileName = @"C:\Document_ADG_Assistance\CM\" + num + " " + contrat.client.Nom + " " + date + ".docx";

					// Sauver le document
					nvDoc.SaveAs(ref fileName, ref missing, ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing, ref missing, ref missing,
                                ref missing, ref missing, ref missing, ref missing, ref missing,
                                ref missing);

                    foreach (Word.Section section in nvDoc.Sections)
                    {
                        Word.Range rng = section.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                        Word.Fields flds = rng.Fields;
                        foreach (Word.Field fld in flds)
                        {
                            fld.Update();
                        }
                    }

                    nvDoc.Save();
                    msWord.Visible = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
            }

        }

        private void CreationBI(string num, BonIntervention bi)
        {
		
			try
			{
                int id = bi.Id;
                if (id == 0)
                {
                    MessageBox.Show("Ce bon d'intervention n'existe pas.");
                    return;
                }

				Word.Document nvDoc;
				// Créer le document

                

				String path = (from word in dbEntity.ModelWord select word.ModelBI).First();

				nvDoc = msWord.Documents.Add(path);


				Replace("<Interlocuteur>", bi.Interlocuteur);
                if (bi.Contrat == null || bi.Contrat.NumContrat == "Hors Contrat")
                {
                    ReplaceBold("<NumeroContrat>", "Hors Contrat");
                    Replace("<Facturable>", "Important : Intervention facturable");
                    Replace("<NombreHeuresContrat> H annuelles du <DateDebut> au <DateFin>", "");
                    Replace("(<HeuresRestantesAvant> heures restantes), ", "");
                    Replace("<HeuresRestantesApres> heures", "Hors Contrat");
                }
                else
                {
                    Replace("<NumeroContrat>", bi.Contrat.NumContrat);
                    Replace("<Facturable>", "");
                    Replace("<NombreHeuresContrat>", bi.Contrat.NbHeures.ToString());
                    Replace("<DateDebut>", bi.Contrat.DateDeb.Value.ToShortDateString());
                    Replace("<DateFin>", bi.Contrat.DateFin.Value.ToShortDateString());
                    Replace("<HeuresRestantesAvant>", (Int32.Parse(bi.Contrat.NbHeures.ToString()) + Int32.Parse(bi.Contrat.HeuresRestantes.ToString())).ToString());
                    Replace("<HeuresRestantesApres>", bi.Contrat.HeuresRestantes.ToString());
                }

				Replace("<NomClient>", bi.Client.Nom);
                Replace("<Adresse>", bi.Client.Adresse);
                if (bi.Client.Complement == "" || bi.Client.Complement == null)
                {
                    msWord.Selection.GoTo(Word.WdGoToItem.wdGoToLine, Word.WdGoToDirection.wdGoToFirst, 1, "");
                    msWord.Selection.Find.Execute("<Complement>");
                    msWord.Selection.Delete();
                }
                else
                {
                    Replace("<Complement>", bi.Client.Complement);
                }

				Replace("<CodePostal>", bi.Client.CodePostal);
                Replace("<Ville>", bi.Client.Ville);
                Replace("<DateSaisie>", bi.Date.ToShortDateString());
                Replace("<NumeroBon>", bi.NumeroBon);

				int sommeHeures = 0;
				LigneIntervention lastLi = bi.LigneIntervention.Last();
				foreach (LigneIntervention li in bi.LigneIntervention)
				{
					sommeHeures += int.Parse(li.NbHeures.ToString());
					if (!li.Equals(lastLi))
					{
						Replace("<Titre>", li.Titre);
						Replace("<Description>", li.Description);
						Replace("<Details>", "Intervention effectuée par " + li.Intervenant + " le " + li.DateIntervention.ToShortDateString());
						Replace("<NombreHeuresIntervnetions>", li.NbHeures + " heures");
						msWord.Selection.MoveDown(Word.WdUnits.wdLine, 2, Word.WdMovementType.wdMove);
						msWord.Selection.MoveRight(Word.WdUnits.wdCharacter, 2, Word.WdMovementType.wdMove);
						msWord.Selection.Paste();
					}
					else
					{
						Replace("<Titre>", li.Titre);
						Replace("<Description>", li.Description);
						Replace("<Details>", "Intervention effectuée par " + li.Intervenant + " le " + li.DateIntervention.ToShortDateString());
						Replace("<NombreHeuresIntervnetions>", li.NbHeures + " heures");
						msWord.ActiveDocument.Tables[2].Rows.Last.Delete();
					}
				}


				Replace("<SommeHeures>", sommeHeures.ToString());

				// Attribuer le nom
				object fileName;
				string dateSaisie = bi.Date.ToShortDateString();

				string date = dateSaisie.Substring(6) + dateSaisie.Substring(3, 2) + dateSaisie.Substring(0, 2);
				if (bi.Contrat == null || bi.Contrat.NumContrat == "Hors Contrat")
				{
					fileName = @"C:\Document_ADG_Assistance\BI\" + num + " " + bi.Client.Nom + " " + date + ".docx";
                } else
                {
					fileName = @"C:\Document_ADG_Assistance\BI\" + num + " " + bi.Contrat.NumContrat + " " + bi.Client.Nom + " "+ date + ".pdf";
                    
                }
                // Sauver le document
                object fileFormat = WdSaveFormat.wdFormatPDF;

                nvDoc.SaveAs(ref fileName, ref fileFormat, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing, ref missing, ref missing, ref missing, ref missing,
                            ref missing);

                msWord.Visible = false;

                var dbParam = dbEntity.Parametre.Find(1);
                var fromAddress = new MailAddress(dbParam.email, "ADG ENGINEERING");
                string fromPassword = dbParam.password;

                MailMessage envoiMail = new MailMessage();
                envoiMail.From = new MailAddress(fromAddress.Address);
                ObjectCache cache = MemoryCache.Default;
                object mailConvert = cache.Get("Mail");
                string mailADG = mailConvert.ToString();

                

                int idContrat = (from contrat in dbEntity.Contrat
                                where contrat.NumContrat == bi.Contrat.NumContrat
                                select contrat.Id).First();

                int idProjet = (int)(from projettocontrat in dbEntity.ProjetToContrat
                               where projettocontrat.idContrat == idContrat
                               select projettocontrat.idProjet).First();

               

                int idClient = (int)(from projet in dbEntity.Projet
                               where projet.Id == idProjet
                               select projet.IdClient).First();

             

                string mailClient = (from clients in dbEntity.Client
                                 where clients.id == idClient
                                 select clients.Mail).First();

               

                

                envoiMail.To.Add(new MailAddress(mailADG));
                envoiMail.To.Add(new MailAddress(mailClient));
                envoiMail.Subject = "Bon Intervention sur votre contrat n°"+bi.Contrat.NumContrat;
                envoiMail.IsBodyHtml = true;
                envoiMail.Body = "";


                LinkedResource LinkedImage = new LinkedResource(@"C:\Document_ADG_Assistance\LogoADG.jpg");
                LinkedImage.ContentId = "MyPic";
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
                "<html><body>Bonjour.<br><br>Veuillez trouver ci-joint votre bon d'intervention suite à votre demande." +
                "<br><br>Nous vous remercions de bien vouloir nous renvoyer une copie signée de ce bon." +
                "<br><br>Pour toute question relative à ce message, merci de bien vouloir nous contacter à l’adresse support@adg-software.fr" +
                "<br><br>Cordialement" +
                "<br><br> <img src=cid:MyPic><br><strong>ADG Software Engineering" +
                "<br><p style=\"color: purple\">2, Boulevard de la Fresnellerie - 72100 LE MANS</p>" +
                "Tel : +33 (0)2 43 43 05 43 / Fax : +33 (0)2 43 24 16 36" +
                "<br><a href=\"www.adg-software.fr\">www.adg-software.fr</a>" +
                "<br><p style=\"color: green\">Be green, leave it on the screen</p></strong>",
                 null, "text/html");
                htmlView.LinkedResources.Add(LinkedImage);
                envoiMail.AlternateViews.Add(htmlView);
                LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);
                

                // création de la pièce jointe
                Attachment PDF_BI = new Attachment(fileName.ToString()); // chemin de la pièce jointe

                // ajout de la pièce jointe au mail
                envoiMail.Attachments.Add(PDF_BI);

                SmtpClient client = new SmtpClient();

                // définition du serveur smtp
                client.Host = dbParam.smtp;

                // définition des login et pwd si smtp sécurisé
                client.Credentials = new NetworkCredential(fromAddress.Address, fromPassword);

                try
                {
                    if(mailClient != "")
                    {
                        client.Send(envoiMail);
                        System.Windows.Forms.NotifyIcon _notifyIcon = new System.Windows.Forms.NotifyIcon();
                        _notifyIcon.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location);
                        _notifyIcon.BalloonTipClosed += (s, e) => _notifyIcon.Visible = false;
                        _notifyIcon.Visible = true;
                        _notifyIcon.ShowBalloonTip(3000, "Bon intervention n°" + bi.NumeroBon, "Le mail vient d'être envoyé aux personnes suivantes:\n - " + mailClient + "\n - " + mailADG, System.Windows.Forms.ToolTipIcon.Info);
                        msWord.Visible = true;
                    }
                    

                }
                catch
                {
                    MessageBox.Show("Une erreur est survenue. Le client n'a pas d'e-mail enregistrer.");
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.StackTrace);
            }
        }



        private void Replace(string find, string replace)
        {
            try
            {
                msWord.Selection.GoTo(Word.WdGoToItem.wdGoToLine, Word.WdGoToDirection.wdGoToFirst, 1, "");
                msWord.Selection.Find.Execute(find);
             
                Word.Find findObject = msWord.Selection.Find;
                findObject.ClearFormatting();
                findObject.Text = find;
                
                findObject.Replacement.ClearFormatting();
                findObject.Replacement.Text = replace;
                
                if("<Titre>" == find)
                {
                    msWord.Selection.SelectRow();
                    msWord.Selection.Copy();
                }

                object replaceAll = Word.WdReplace.wdReplaceAll;
                findObject.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref replaceAll, ref missing, ref missing, ref missing, ref missing);
            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
            }

        }
        private void ReplaceHeader(string find, string replace, Word.Document nvDoc)
        {

            try
            {
                object replaceAll = Word.WdReplace.wdReplaceAll;

                foreach (Word.Section section in nvDoc.Sections)
                {
                    Word.Range headerRange = section.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Find.Text = find;
                    headerRange.Find.Replacement.Text = replace;
                    headerRange.Find.Execute(ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, 
                        ref missing, ref missing, ref missing, ref missing, ref replaceAll, ref missing, ref missing, ref missing, ref missing);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
            }

        }

        private void ReplaceBold(string find, string replace)
        {
            try
            {
                msWord.Selection.GoTo(Word.WdGoToItem.wdGoToLine, Word.WdGoToDirection.wdGoToFirst, 1, "");
                msWord.Selection.Find.Execute(find);
                msWord.Selection.Font.Bold = 1;

                Word.Find findObject = msWord.Selection.Find;
                findObject.ClearFormatting();
                findObject.Text = find;

                findObject.Replacement.ClearFormatting();
                findObject.Replacement.Text = replace;

                if ("<Titre>" == find)
                {
                    msWord.Selection.SelectRow();
                    msWord.Selection.Copy();
                }

                object replaceAll = Word.WdReplace.wdReplaceAll;
                findObject.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing, ref missing,
                    ref replaceAll, ref missing, ref missing, ref missing, ref missing);
            }
            catch (Exception ex)
            {
                MessageBox.Show("DocWord" + "." + System.Reflection.MethodBase.GetCurrentMethod().Name + " : " + ex.Message);
            }
        }
    }
}
