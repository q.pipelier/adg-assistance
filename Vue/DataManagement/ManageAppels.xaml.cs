﻿using ADG_Assistance.Functions;
using ADG_Assistance.DB;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using NLog;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.Caching;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManageAppel.xaml
	/// </summary>
	public abstract partial class ManageAppel : Window
	{
        protected Logger Log = LogManager.GetCurrentClassLogger();
        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		private Boolean traite = false;
		private int idColor = 1;
		private Appel oAppelSel;
        private Appel appelCopy;
        private Boolean appelIsModif;
        private Boolean appelIsCopy;
        private Boolean isCreated = false;

        List<int> destinataireChoose = new List<int>();
        List<int> destinataireChooseModif = new List<int>();

        protected ManageAppel(Appel appel, Boolean isModif, Boolean isCopy)
		{
			InitializeComponent();
			oAppelSel = appel;
			appelIsModif = isModif;
            appelIsCopy = isCopy;

            if (!appelIsCopy && !appelIsModif)
            {
                oAppelSel = new Appel
                {
                    NomClient = ""
                };

                dbEntity.Appel.Add(oAppelSel);
                dbEntity.SaveChanges();

            }

            

            if (appelIsModif || appelIsCopy)
			{
				FillFields();
                destinataireChooseModif.AddRange(destinataireChoose);
			}
			else
			{
				DPK_SaveDate.Text = DateTime.Now.ToString();
			}
			setTraite();
			BgColor();
		}

  

        private void FillFields()
		{
			TXT_CommentaireAppel.Text = oAppelSel.Commentaire;
			TXT_CommentaireTraitement.Text = oAppelSel.detailsTraite;
			TXT_NomClient.Text = oAppelSel.NomClient;
			TXT_tel.Text = oAppelSel.Tel;
			CBX_Traite.IsChecked = oAppelSel.Traitement;

            
            
			CMB_Priorite.Text = oAppelSel.Priorite;
			CMB_Projet.Text = oAppelSel.Projet;
			CMB_NomContactEntreprise.Text = oAppelSel.NomContact;
			DPK_SaveDate.Text = oAppelSel.DateCreation.ToString();
			DPK_treatmentDate.Text = oAppelSel.Traitement.ToString();
            
		}

        private void BTN_Add_Click(object sender, RoutedEventArgs e)
        {

           
            saveAppel();
        }



        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void CBX_Traite_Checked(object sender, RoutedEventArgs e)
		{
			traite = true;
			setTraite();
		}

		private void saveAppel()
		{

            var connectionString = Function.connectDatabase();
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            if (TXT_NomClient.Text == "" || TXT_tel.Text == "" || CMB_Priorite.SelectedItem.ToString() == "")
			{
				MessageBox.Show("Merci de remplir les champs précédés d'un '*'");
			}
			else
			{
				var appel = dbEntity.Set<Appel>();
                var appelToMembre = dbEntity.Set<T_AppelMembres>();
				DateTime dateSave = (DateTime) DPK_SaveDate.SelectedDate;

				

                if (appelIsCopy)
                {
                    

                    if (traite)
                    {
                        DateTime dateTreatment = (DateTime)DPK_treatmentDate.SelectedDate;
                        oAppelSel.NomContact = CMB_NomContactEntreprise.Text;
                        oAppelSel.NomClient = TXT_NomClient.Text;
                        oAppelSel.Projet = CMB_Projet.Text;
                        oAppelSel.Tel = TXT_tel.Text;
                        oAppelSel.Traitement = CBX_Traite.IsChecked;
                        oAppelSel.Priorite = ((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString();
                        oAppelSel.detailsTraite = TXT_CommentaireTraitement.Text;
                        oAppelSel.Commentaire = TXT_CommentaireAppel.Text;
                        oAppelSel.DateCreation = dateSave;
                        oAppelSel.DateTraitement = dateTreatment;
                        oAppelSel.IdCouleur = idColor;
                        
                    }
                    else
                    {
                        
                        appelCopy = new Appel{
                            NomContact = CMB_NomContactEntreprise.Text,
                            NomClient = TXT_NomClient.Text,
                            Projet = CMB_Projet.Text,
                            Tel = TXT_tel.Text,
                            Traitement = CBX_Traite.IsChecked,
                            Priorite = ((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString(),
                            Commentaire = TXT_CommentaireAppel.Text,
                            DateCreation = dateSave,

                            IdCouleur = idColor
                        };

                        dbEntity.Appel.Add(appelCopy);
                    }
                    foreach (int items in destinataireChoose)
                    {
                        appelToMembre.Add(new T_AppelMembres
                        {
                            idAppel = appelCopy.idAppel,
                            idMembres = items

                        });
                    }
                    if (CBX_MailSent.IsChecked == true)
                    {
                        sendMail();
                    }
                }
                else
                {
                    if (appelIsModif)
                    {
                       
                        if ((bool)CBX_Traite.IsChecked)
                        {
                            
                            DateTime dateTreatment = (DateTime)DPK_treatmentDate.SelectedDate;

                            
                            
                            try
                            {
                                
                                SqlCommand reqUpdateAppel = new SqlCommand();
                                reqUpdateAppel.Connection = con;
                                reqUpdateAppel.CommandText = "UPDATE Appel SET NomContact = @nomContact, NomClient = @nomClient, Projet = @projet, Tel = @tel, Traitement = @traite, Priorite = @prio" +
                                    ", Commentaire = @comment, DateCreation = @dateCrea, IdCouleur = @idColor, DateTraitement = @dateTraite, detailsTraite = @detailTraite WHERE idAppel = @id";
                                reqUpdateAppel.Parameters.AddWithValue("@nomContact", CMB_NomContactEntreprise.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@nomClient", TXT_NomClient.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@projet", CMB_Projet.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@tel", TXT_tel.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@traite", CBX_Traite.IsChecked);
                                reqUpdateAppel.Parameters.AddWithValue("@prio", CMB_Priorite.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@comment", TXT_CommentaireAppel.Text);
                                reqUpdateAppel.Parameters.AddWithValue("@dateCrea", dateSave);
                                reqUpdateAppel.Parameters.AddWithValue("@idColor", idColor);
                                reqUpdateAppel.Parameters.AddWithValue("@dateTraite", dateTreatment);
                                reqUpdateAppel.Parameters.AddWithValue("@id", oAppelSel.idAppel);
                                reqUpdateAppel.Parameters.AddWithValue("@detailTraite", oAppelSel.detailsTraite);

                                reqUpdateAppel.ExecuteNonQuery();

                            }
                            catch
                            {

                            }
                        }
                        else
                        {
                            try
                            {

                                SqlCommand reqUpdateAppelTraite = new SqlCommand();
                                reqUpdateAppelTraite.Connection = con;
                                reqUpdateAppelTraite.CommandText = "UPDATE Appel SET NomContact = @nomContact, NomClient = @nomClient, Projet = @projet, Tel = @tel, Traitement = @traite, Priorite = @prio" +
                                    ", Commentaire = @comment, DateCreation = @dateCrea, IdCouleur = @idColor, DateTraitement = @dateTraite WHERE idAppel = @id";
                                reqUpdateAppelTraite.Parameters.AddWithValue("@nomContact", CMB_NomContactEntreprise.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@nomClient", TXT_NomClient.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@projet", CMB_Projet.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@tel", TXT_tel.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@traite", CBX_Traite.IsChecked);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@prio", CMB_Priorite.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@comment", TXT_CommentaireAppel.Text);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@dateCrea", dateSave);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@idColor", idColor);
                                reqUpdateAppelTraite.Parameters.AddWithValue("@id", oAppelSel.idAppel);




                                reqUpdateAppelTraite.ExecuteNonQuery();

                            }
                            catch
                            {

                            }
                        }

                        
                        try
                        {
                            
                            SqlCommand reqUpdateHeure = new SqlCommand();
                            reqUpdateHeure.Connection = con;
                            reqUpdateHeure.CommandText = "DELETE FROM T_AppelMembres WHERE idAppel = @id";
                            reqUpdateHeure.Parameters.AddWithValue("@id", oAppelSel.idAppel);



                            reqUpdateHeure.ExecuteNonQuery();

                        }
                        catch
                        {

                        }


                        foreach (int items in destinataireChoose)
                        {

                            

                            appelToMembre.Add(new T_AppelMembres
                            {
                                idAppel = oAppelSel.idAppel,
                                idMembres = items

                            });
                        }

                        
                    }
                    else
                    {

                        // AJOUT

                        if (traite)
                        {
                            DateTime dateTreatment = (DateTime)DPK_treatmentDate.SelectedDate;

                            oAppelSel.NomContact = CMB_NomContactEntreprise.Text;
                                oAppelSel.NomClient = TXT_NomClient.Text;
                                oAppelSel.Projet = CMB_Projet.Text;
                                oAppelSel.Tel = TXT_tel.Text;
                                oAppelSel.Traitement = CBX_Traite.IsChecked;
                                oAppelSel.Priorite = ((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString();
                                oAppelSel.detailsTraite = TXT_CommentaireTraitement.Text;
                                oAppelSel.Commentaire = TXT_CommentaireAppel.Text;
                                oAppelSel.DateCreation = dateSave;
                            
                                oAppelSel.DateTraitement = dateTreatment;

                            oAppelSel.IdCouleur = idColor;
                            
                        }
                        else
                        {
                           
                                oAppelSel.NomContact = CMB_NomContactEntreprise.Text;
                            oAppelSel.NomClient = TXT_NomClient.Text;
                            oAppelSel.Projet = CMB_Projet.Text;
                            oAppelSel.Tel = TXT_tel.Text;
                            oAppelSel.Traitement = CBX_Traite.IsChecked;
                            oAppelSel.Priorite = ((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString();
                            oAppelSel.Commentaire = TXT_CommentaireAppel.Text;
                            oAppelSel.DateCreation = dateSave;
                            
                            oAppelSel.IdCouleur = idColor;
                            
                        }

                        foreach(int items in destinataireChoose)
                        {
                            appelToMembre.Add(new T_AppelMembres
                            {
                                idAppel = oAppelSel.idAppel,
                                idMembres = items

                            });
                        }

                        if (CBX_MailSent.IsChecked == true)
                        {
                            sendMail();
                        }
                    }
                }
                
                isCreated = true;
                dbEntity.SaveChanges();
				ToUpdateParent();
				Close();
			}
		}	

		internal abstract void ToUpdateParent();

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			dbEntity.Membres.Load();
			LB_Destinataire.ItemsSource = (dbEntity.Membres.Local).ToList();
			
		}		

		private void searchAutoFillFields()
		{
			dbEntity.Client.Load();
			var userChoice = dbEntity.Client.Where(c => c.Nom.Contains(TXT_NomClient.Text)).ToList();

			if (userChoice != null)
			{
				foreach (Client item in userChoice)
				{
					if (item.Nom == CMB_NomClient.Text)
					{
						CMB_Projet.Items.Clear();
						CMB_NomContactEntreprise.Items.Clear();
						TXT_tel.Text = item.Numero;
						foreach (Projet projet in item.Projet.ToList())
						{
							CMB_Projet.Items.Add(projet.Nom);
						}
						foreach (Contact clientContact in item.Contact.ToList())
						{
							CMB_NomContactEntreprise.Items.Add(clientContact.Prenom + " " + clientContact.Nom);
						}
					}
				}
				try
				{
                    if (CMB_Projet.Items.Count > 0)
                    {
                        CMB_Projet.SelectedItem = CMB_Projet.Items[0];
                    }
                    if(CMB_Projet.Items.Count > 0)
					    CMB_NomContactEntreprise.SelectedItem = CMB_NomContactEntreprise.Items[0];
				}
				catch (Exception ex)
				{
                    Function.AfficheException(ex, "Une erreur est survenue lors de l'affichage des projets : ", "Appel", Log);
                }
			}

			if (TXT_NomClient.Text == "")
			{
				CMB_Projet.Items.Clear();
				CMB_NomContactEntreprise.Items.Clear();
				TXT_tel.Text = "";
			}	
		}

		private void setTraite()
		{
			if (traite)
			{
				TXT_CommentaireTraitement.IsEnabled = true;
				DPK_treatmentDate.IsEnabled = true;
				DPK_treatmentDate.Text = DateTime.Now.ToString();
			}
			else
			{
				TXT_CommentaireTraitement.IsEnabled = false;
				DPK_treatmentDate.IsEnabled = false;
			}

			BgColor();
		}

		private void CBX_Traite_Unchecked(object sender, RoutedEventArgs e)
		{
			traite = false;
			setTraite();
		}

		private void sendMail()
		{

            var dbParam = dbEntity.Parametre.Find(1);
            var fromAddress = new MailAddress(dbParam.email, "ADG ENGINEERING");
            string fromPassword = dbParam.password;
            var dbMail = dbEntity.Mail.Find(1);

            MailMessage envoiMail = new MailMessage();
            envoiMail.From = new MailAddress(fromAddress.Address);
            
            
            for(int i = 0; i < destinataireChoose.Count; i++)
            {

                int desti = destinataireChoose[i];

                var reqDestiMail = from membres in dbEntity.Membres
                                   where membres.id == desti
                                   select membres.mail;

                foreach(var mail in reqDestiMail)
                {
                    
                    envoiMail.To.Add(new MailAddress(mail));
                }
            }

            envoiMail.Subject = getDataMail(dbMail.Subject);
            envoiMail.Body = getDataMail(dbMail.Body);

            SmtpClient client = new SmtpClient();

            // définition du serveur smtp
            client.Host = dbParam.smtp;

            // définition des login et pwd si smtp sécurisé
            client.Credentials = new NetworkCredential(fromAddress.Address, fromPassword);

            try
            {
                client.Send(envoiMail);
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue.");
            }


        }

        private void BgColor()
		{
			var colorFaible = dbEntity.ColorAppel.Find(1);
			var colorMoyenne = dbEntity.ColorAppel.Find(2);
			var colorImportante = dbEntity.ColorAppel.Find(3);
			var colorTrate = dbEntity.ColorAppel.Find(4);

            
            try
            {
                if (!traite)
                {
                    switch (((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString())
                    {
                        case "Faible":
                            this.Background = new BrushConverter().ConvertFromString(colorFaible.Couleur) as SolidColorBrush;
                            idColor = 1;
                            break;

                        case "Moyenne":
                            this.Background = new BrushConverter().ConvertFromString(colorMoyenne.Couleur) as SolidColorBrush;
                            idColor = 2;
                            break;

                        case "Importante":
                            this.Background = new BrushConverter().ConvertFromString(colorImportante.Couleur) as SolidColorBrush;
                            idColor = 3;
                            break;
                    }
                }
                else
                {
                    this.Background = new BrushConverter().ConvertFromString(colorTrate.Couleur) as SolidColorBrush;
                    idColor = 4;
                }
            }
            catch { }
            
		}

		private void CMB_Priorite_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			BgColor();
		}

		private string getDataMail(string mailString)
		{

			string temp = mailString;

			if (mailString.Contains("%ContactName%"))
			{
				try
				{
					temp = temp.Replace("%ContactName%", CMB_NomContactEntreprise.Text);
				}
				catch (Exception)
				{
					temp = temp.Replace("%ContactName%", " - ");
				}
			}
			if (mailString.Contains("%CompanyName%"))
			{
				try
				{
					temp = temp.Replace("%CompanyName%", TXT_NomClient.Text);
				}
				catch (Exception)
				{
					temp = temp.Replace("%CompanyName%", " - ");
				}
			}
			if (mailString.Contains("%ContactPhone%"))
			{
				try
				{
					temp = temp.Replace("%ContactPhone%", TXT_tel.Text);
				}
				catch (Exception)
				{
					temp = temp.Replace("%ContactPhone%", " - ");
				}
			}
			if (mailString.Contains("%ProjectName%"))
			{
				try
				{
					temp = temp.Replace("%ProjectName%", CMB_Projet.SelectedItem.ToString());
				}
				catch (Exception)
				{
					temp = temp.Replace("%ProjectName%", " - ");
				}
			}
			if (mailString.Contains("%Priority%"))
			{
				try
				{
					temp = temp.Replace("%Priority%", ((ComboBoxItem)CMB_Priorite.SelectedItem).Content.ToString());
				}
				catch (Exception)
				{
					temp = temp.Replace("%Priority%", " - ");
				}
			}
			if (mailString.Contains("%StartDate%"))
			{
				try
				{
					temp = temp.Replace("%StartDate%", DPK_SaveDate.ToString());
				}
				catch (Exception)
				{
					temp = temp.Replace("%StartDate%", " - ");
				}
			}
			if (mailString.Contains("%TreatmentDate%"))
			{
				try
				{
					temp = temp.Replace("%TreatmentDate%", DPK_treatmentDate.ToString());
				}
				catch (Exception)
				{
					temp = temp.Replace("%TreatmentDate%", " - ");
				}
			}
			if (mailString.Contains("%Comment%"))
			{
				try
				{
					temp = temp.Replace("%Comment%", TXT_CommentaireAppel.Text);
				}
				catch (Exception)
				{
					temp = temp.Replace("%Comment%", " - ");
				}
			}

			return temp;
		}

		private void TXT_NomClient_KeyUp(object sender, KeyEventArgs e)
		{

			if (CMB_NomClient.Text != TXT_NomClient.Text || TXT_NomClient.Text == "")
			{
				CMB_NomClient.Items.Clear();
			}
			else
			{
				searchAutoFillFields();
			}

			CMB_NomClient.IsDropDownOpen = true;
			dbEntity.Client.Load();
			var clients = dbEntity.Client.Local;


			foreach (Client client in clients)
			{
				if (client.Nom.ToLower().StartsWith(TXT_NomClient.Text.ToLower()))
				{
					CMB_NomClient.Items.Add(client.Nom);
				}
			}

			if (CMB_NomClient.Items.Count == 0)
			{
				CMB_NomClient.IsDropDownOpen = false;
			}

			if (e.Key == Key.Enter || TXT_NomClient.Text == "")
			{
				searchAutoFillFields();
			}
		}

		private void TXT_NomClient_TextChanged(object sender, TextChangedEventArgs e)
		{
			searchAutoFillFields();
		}

        

        private void LB_Destinataire_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;

            

            if (cb != null)
            {
               
                    var reqIdMembre = from membres in dbEntity.Membres
                                      where membres.FullName == cb.Content.ToString()
                                      select membres.id;

                    foreach (var result in reqIdMembre)
                    {
                        destinataireChoose.Add(result);
                    }
                
                
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!isCreated)
            {
                var connectionString = Function.connectDatabase();
                SqlConnection con = new SqlConnection(connectionString);
                try
                {
                    con.Open();
                    SqlCommand reqUpdateHeure = new SqlCommand();
                    reqUpdateHeure.Connection = con;
                    reqUpdateHeure.CommandText = "DELETE FROM Appel WHERE idAppel = @id";
                    reqUpdateHeure.Parameters.AddWithValue("@id", oAppelSel.idAppel);
                   


                    reqUpdateHeure.ExecuteNonQuery();

                }
                catch
                {

                }
            }
        }

        private void CBX_Destinataire_Loaded(object sender, RoutedEventArgs e)
        {

            CheckBox cb = sender as CheckBox;

            if (appelIsModif || appelIsCopy)
            {
                var reqDestinataire = from appeltomembre in dbEntity.T_AppelMembres
                                      where appeltomembre.idAppel == oAppelSel.idAppel
                                      select appeltomembre.idMembres;



                foreach (var result in reqDestinataire)
                {

                    var reqNomMembre = from membres in dbEntity.Membres
                                       where membres.id == result
                                       select membres.FullName;

                    foreach (var fullname in reqNomMembre)
                    {
                        if(cb.Content.ToString() == fullname)
                        {
                            cb.IsChecked = true;
                        }
                    }

                }

                
            }
        }

        private void CBX_Destinataire_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;



            if (cb != null)
            {
                
            
                    var reqIdMembre = from membres in dbEntity.Membres
                                      where membres.FullName == cb.Content.ToString()
                                      select membres.id;

                    foreach (var result in reqIdMembre)
                    {
                        destinataireChoose.Remove(result);
                    }
                

            }
        }
    }
}