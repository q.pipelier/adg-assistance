﻿using ADG_Assistance.DB;
using ADG_Assistance.Functions;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour SelectProjet.xaml
	/// </summary>
	public partial class SelectProjet : Window
	{

		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		public Projet oProjetSel { get; set; }
		private Contrat oContratSel;
		private UIElement parentContrat;
		MainWindow m_oWin;

		public SelectProjet(UIElement parent, Contrat contrat, MainWindow oWin, ADG_AssistanceEntities Entity)
		{
			InitializeComponent();
			parentContrat = parent;
			oContratSel = contrat;
			m_oWin = oWin;

            if (Entity != null)
                dbEntity = Entity;
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			ToUpdate();
		}

		private void BTN_AddProjet_Click(object sender, RoutedEventArgs e)
		{
			ManageProjet projet = new ManageProjet(oProjetSel, false, dbEntity, m_oWin, this);
			projet.Show();
		}

		private void BTN_ModifyProjet_Click(object sender, RoutedEventArgs e)
		{
			oProjetSel = (Projet)DG_Projet.SelectedItem;
			if (oProjetSel != null)
			{
				ManageProjet projet = new ManageProjet(oProjetSel, true, dbEntity, m_oWin, this);
				projet.Show();
			}
		}

		private void BTN_Accept_Click(object sender, RoutedEventArgs e)
		{
			oProjetSel = (Projet)DG_Projet.SelectedItem;


            this.Close();
        }

		private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
		{
            oProjetSel = null;

            Close();
		}

		public void ToUpdate()
		{
			dbEntity.SaveChanges();

            int? IdClientContrat = null;

            
            if(oContratSel.Projet.Any())
                IdClientContrat = oContratSel.Projet.First().IdClient;

            //var qProjet = from proj in dbEntity.Projet where proj.IdContrat == null && (!IdClientContrat.HasValue || IdClientContrat == proj.IdClient)  select proj;
            var qProjet = from proj in dbEntity.Projet select proj;
            DG_Projet.ItemsSource = qProjet.ToList();

		}

	}
}