﻿using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataDisplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataManagement
{
	/// <summary>
	/// Logique d'interaction pour ManagePersonnel.xaml
	/// </summary>
	public partial class ManagePersonnel : Window
	{

		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		private DisplayPersonnel parentPersonnel;
		private Membres oMembreSel;

		public ManagePersonnel(DisplayPersonnel personnel)
		{
			InitializeComponent();
			parentPersonnel = personnel;
            BTN_ChangePassword.Visibility = Visibility.Hidden;
		}

		public ManagePersonnel(DisplayPersonnel personnel, Membres membresSel)
		{
			InitializeComponent();
			parentPersonnel = personnel;
			oMembreSel = membresSel;
			getDataPersonnel();
            BTN_ChangePassword.Visibility = Visibility.Visible;
        }


		private void BTN_Ok_Click(object sender, RoutedEventArgs e)
		{
			savePersonnel();
		}

		private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Vérifie champs remplis + ajoute/Modifie Personnel en base
		/// </summary>
		private void savePersonnel()
		{
			if (TXT_PersonnelFirstName.Text == "" || CBB_Role.Text == "" || TXT_PersonnelMail.Text == "" || TXT_PersonnelLastName.Text == "" )
			{

                MessageBox.Show("Les champs précédes d'un \" * \" sont obligatoires");
            }
			else
			{

                string username = TXT_PersonnelFirstName.Text.ToLower() + "." + TXT_PersonnelLastName.Text.ToLower();
                string fullName = TXT_PersonnelFirstName.Text + " " + TXT_PersonnelLastName.Text;
                if (!parentPersonnel.IsModify)
				{

                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hash = GetMd5Hash(md5Hash, TXT_Password.Password);
                        
                        var membres = dbEntity.Set<Membres>();
                        membres.Add(new Membres
                        {

                            mail = TXT_PersonnelMail.Text,
                            nom = TXT_PersonnelLastName.Text,
                            prenom = TXT_PersonnelFirstName.Text,
                            role = CBB_Role.Text,
                            password = hash,
                            tel = TXT_PersonnelPhone.Text,
                            user = username,
                            civilite = CBB_PersonnelCivilite.Text,
                            FullName = fullName,
                            FirstConnexion = false


                        });


                    }

                   
				}
				else
				{
                    

                    if (TXT_Password.IsVisible)
                    {
                        if (TXT_Password.Password == "")
                        {
                            MessageBox.Show("Les champs précédes d'un \" * \" sont obligatoires");
                        }
                        else
                        {
                            using (MD5 md5Hash = MD5.Create())
                            {
                                string hash = GetMd5Hash(md5Hash, TXT_Password.Password);
                                oMembreSel.password = hash;
                            }
                        }
                    }

                    oMembreSel.mail = TXT_PersonnelMail.Text;
                    oMembreSel.nom = TXT_PersonnelLastName.Text;
                    oMembreSel.prenom = TXT_PersonnelFirstName.Text;
                    oMembreSel.role = CBB_Role.Text;
                    oMembreSel.tel = TXT_PersonnelPhone.Text;
                    oMembreSel.user = username;
                    oMembreSel.civilite = CBB_PersonnelCivilite.Text;

                }
				dbEntity.SaveChanges();
				this.Close();
				parentPersonnel.ToUpdate();
			}

		}

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// <summary>
        /// Vérifie qu'un item est sélectionné puis récupère les données de la personne 
        /// </summary>
        private void getDataPersonnel()
		{
			if (oMembreSel != null)
			{
				parentPersonnel.IsModify = true;

				TXT_PersonnelFirstName.Text = oMembreSel.prenom;
				TXT_PersonnelLastName.Text = oMembreSel.nom;
				TXT_PersonnelPhone.Text = oMembreSel.tel;
				TXT_PersonnelMail.Text = oMembreSel.mail;
				CBB_PersonnelCivilite.Text = oMembreSel.civilite;
                CBB_Role.Text = oMembreSel.role;
                TXT_Password.Visibility = Visibility.Collapsed;
                LBL_Password.Visibility = Visibility.Collapsed;
			}
		}

        private void BTN_ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            TXT_Password.Visibility = Visibility.Visible;
            LBL_Password.Visibility = Visibility.Visible;
        }
    }
}
