﻿using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ADG_Assistance.Vue.DataDisplay
{
	/// <summary>
	/// Logique d'interaction pour DisplayContact.xaml
	/// </summary>
	public partial class DisplayContact : Page
	{
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
		public Boolean ToModif { get; set; }
		public object selectedContact
        {
            get; set;
        }
        private IQueryable<Contact> qContact;
		MainWindow m_oWin;

		public DisplayContact(MainWindow oWin)
		{
            ToModif = false;

            InitializeComponent();
			m_oWin = oWin;
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			ToUpdate();
			m_oWin.Title = this.Title;
		}

		private void BTN_AddContact_Click(object sender, RoutedEventArgs e)
		{
			ToModif = false;
			ManageContact contact = new ManageContact(this, selectedContact, dbEntity, false, null);
			contact.Show();
		}

		private void BTN_ModifyContact_Click(object sender, RoutedEventArgs e)
		{
			ModifyContrat();
		}

		private void ModifyContrat()
		{
			selectedContact = DG_Contact.SelectedItem;
			ToModif = true;
			if (selectedContact != null)
			{
				ManageContact contact = new ManageContact(this, selectedContact, dbEntity, false, null);
				contact.Show();
			}
		}

		private void DG_Contact_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			ModifyContrat();
		}

		private void BTN_DeleteContact_Click(object sender, RoutedEventArgs e)
		{
			Deletecontact();
		}

		public void Deletecontact()
		{ 
			if (DG_Contact.SelectedItem == null)
			{
				MessageBox.Show("Merci de sélectionner un contact");
			}
			else
			{
				try
				{
					Contact contactSelect = DG_Contact.SelectedItem as Contact;
					MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer le contact " + contactSelect.fullName + " de la société " + contactSelect.Client.Nom + " ?", "Alerte supression", MessageBoxButton.YesNo);
					if (dr == MessageBoxResult.Yes)
					{
						IQueryable< ContactProjet> lst = (from contratProjet in dbEntity.ContactProjet where contratProjet.IdContact == contactSelect.Id select contratProjet);

						if (!lst.Any())
						{
							dbEntity.Contact.Remove(contactSelect);
							dbEntity.SaveChanges();
							ToUpdate();
						}
						else
							MessageBox.Show("Impossible de supprimer ce contact, il est lié à un projet, vérifiez la table ContactProjet");

					}
				}
				catch (Exception ex)
				{

					MessageBox.Show("Une erreur est survenue lors de la suppression du contrat : " + ex.Message);
				}				
			}
		}
        ListSortDirection sortDirection;
        public void ToUpdate()
		{
			qContact = from contact in dbEntity.Contact select contact;

            if (!string.IsNullOrEmpty(TXT_FilterClientName.Text))
            {
                qContact = qContact.Where(x => x.Client != null && x.Client.Nom.ToUpper().Contains(TXT_FilterClientName.Text.ToUpper()));
            }

            if (!string.IsNullOrEmpty(TXT_FilterContactFirstName.Text))
            {
                qContact = qContact.Where(x => x.Prenom.ToUpper().Contains(TXT_FilterContactFirstName.Text.ToUpper()));
            }

            if (!string.IsNullOrEmpty(TXT_FilterContactLastName.Text))
            {
                qContact = qContact.Where(x => x.Nom.ToUpper().Contains(TXT_FilterContactLastName.Text.ToUpper()));
            }

            if (!string.IsNullOrEmpty(TXT_FilterContactTel.Text))
            {
                qContact = qContact.Where(x => x.Telephone.ToUpper().Contains(TXT_FilterContactTel.Text.ToUpper()));
            }


            DG_Contact.ItemsSource = qContact.ToList();
            DG_Contact.Items.Refresh();
            applySortDescriptions(sortDirection);
		}



		private void BTN_ClearFilters_Click(object sender, RoutedEventArgs e)
		{
			TXT_FilterClientName.Text = "";
			TXT_FilterContactFirstName.Text = "";
			TXT_FilterContactLastName.Text = "";
			TXT_FilterContactTel.Text = "";
		}

		private void BTN_Research_Click(object sender, RoutedEventArgs e)
		{
			ToUpdate();
		}

		private void CheckEnterPressed(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}
        int selectedColumnIndex;
        private void DG_Contact_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = (e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);

        }
        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            DG_Contact.Items.SortDescriptions.Clear();
            string propertyName = DG_Contact.Columns[selectedColumnIndex].SortMemberPath;
            DG_Contact.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));
            applySortDirection(listSortDirection);
            DG_Contact.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_Contact.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }
    }
}
