﻿using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.DataDisplay
{
	/// <summary>
	/// Logique d'interaction pour DisplayClient.xaml
	/// </summary>
	public partial class DisplayClient : Page
	{
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        public Boolean IsModif {get; set;}
		Client oClientSel;
		private IQueryable<Client> qClient;
		private List<Client> listClientFiltered = new List<Client>();
		MainWindow m_oWin;

		public DisplayClient(MainWindow oWin)
		{
			InitializeComponent();
			m_oWin = oWin;
            IsModif = false;
        }

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			ToUpdate();
			m_oWin.Title = this.Title;
            IsModif = false;
        }

		private void BTN_AddClient_Click(object sender, RoutedEventArgs e)
		{
			IsModif = false;
            
            Client oNewClient = new Client();
            oNewClient.intialise();
            dbEntity.Client.Add(oNewClient);
            dbEntity.SaveChanges();
            
			ManageClient manageClient = new ManageClient(oNewClient, false, "",m_oWin, dbEntity, this);
			manageClient.Show();
		}

		private void BTN_ModifyClient_Click(object sender, RoutedEventArgs e)
		{
			ModifyClient();
		}

		private void DG_Client_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			ModifyClient();
		}

		private void ModifyClient()
		{
			IsModif = true;
			oClientSel = (Client)DG_Client.SelectedItem;
			ManageClient manageClient = new ManageClient(/*this,*/ oClientSel, true, "",m_oWin, dbEntity, this);
			manageClient.Show();
		}

		private void BTN_DeleteClient_Click(object sender, RoutedEventArgs e)
		{
			oClientSel = (Client)DG_Client.SelectedItem;
			DeleteClient();
		}

		private void DeleteClient()
		{
			try
			{

			if (oClientSel != null)
			{
				MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer le client " + oClientSel.Nom, "Alerte supression", MessageBoxButton.YesNo);

				if (dr == MessageBoxResult.Yes)
				{
					dbEntity.Client.Remove(oClientSel);
					dbEntity.SaveChanges();
					ToUpdate();
				}
			}
			else
			{
					MessageBox.Show("Merci de sélectionner un client");
			}

			}
			catch (Exception)
			{
				MessageBox.Show("Impossible de supprimer le client, il est lié à d'autres données");
			}
		}
        ListSortDirection sortDirection;
        public void ToUpdate()
		{
			qClient = from client in dbEntity.Client select client;
			Filters();
			DG_Client.ItemsSource = listClientFiltered;
			DG_Client.Items.Refresh();
            applySortDescriptions(sortDirection);
		}

		private void Filters()
		{
			listClientFiltered.Clear();

			if (qClient.ToList() != null)
			{
				foreach (Client client in qClient)
				{
					if (client.Ville.ToUpper().Contains(TXT_VilleEntreprise.Text.ToUpper()))
					{
						if (client.Nom.ToUpper().Contains(TXT_NomEntreprise.Text.ToUpper()))
						{
							listClientFiltered.Add(client);
						}
					}
				}
			}
		}

		private void BTN_ClearFilters_Click(object sender, RoutedEventArgs e)
		{
			TXT_NomEntreprise.Text = "";
			TXT_VilleEntreprise.Text = "";
		}

		private void BTN_Rechercher_Click(object sender, RoutedEventArgs e)
		{
			ToUpdate();
		}

		private void CheckEnterPressed(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}
        int selectedColumnIndex;
        private void DG_Client_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = (e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);

        }

        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            DG_Client.Items.SortDescriptions.Clear();
            string propertyName = DG_Client.Columns[selectedColumnIndex].SortMemberPath;
            DG_Client.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));
            applySortDirection(listSortDirection);
            DG_Client.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_Client.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }
    }
}
