﻿using ADG_Assistance.Functions;
using ADG_Assistance.DB;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;
using System.ComponentModel;
using System.Data.SqlClient;

namespace ADG_Assistance.Vue
{
	/// <summary>
	/// Logique d'interaction pour DisplayAppel.xaml	
	/// </summary>
	public partial class DisplayAppel : Page
	{
		private ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        readonly private Logger Log = LogManager.GetCurrentClassLogger();
        private IQueryable<Appel> qAppel;
		private DateTime? filterDateDebut = DateTime.Now.AddYears(-10);
		private DateTime? filterDateFin = DateTime.Now.AddYears(10);
		private Boolean? filterTraitement = null;
		private String filterPriorite = "";
		private List<AffichageAppel> listAppelFiltered = new List<AffichageAppel>();
		private Appel oAppelSel;
        private List<AffichageAppel> appelsDisplay = new List<AffichageAppel>();

		public DisplayAppel(MainWindow oWin)
		{
            
			InitializeComponent();
         
            oWin.Title = this.Title;
			CBB_FullNameDestinataire.ItemsSource = (from membres in dbEntity.Membres select membres.FullName).ToList();
		}

		public void Page_Loaded(object sender, RoutedEventArgs e)
		{
   
            ToUpdate();
		}
		
		private void Filters()
		{
			listAppelFiltered.Clear();
            

            if (appelsDisplay != null)
			{

				if ((Boolean)RBTN_PrioFaible.IsChecked)
				{
					filterPriorite = RBTN_PrioFaible.Content.ToString();
				}
				if ((Boolean)RBTN_PrioMoyenne.IsChecked)
				{
					filterPriorite = RBTN_PrioMoyenne.Content.ToString();
				}
				if ((Boolean)RBTN_PrioImportante.IsChecked)
				{
					filterPriorite = RBTN_PrioImportante.Content.ToString();
				}
				if ((Boolean)RBTN_PrioNone.IsChecked)
				{
					filterPriorite = "";
				}

				if ((Boolean)RBTN_TraiteNone.IsChecked)
				{
					filterTraitement = null;
				}
				if ((Boolean)RBTN_NonTraite.IsChecked)
				{
					filterTraitement = false;
				}
				if ((Boolean)RBTN_Traite.IsChecked)
				{
					filterTraitement = true;
				}

				filterDateDebut = Function.stringToDate(DPK_DebutDateAppel.Text);
				filterDateFin = Function.stringToDate(DPK_FinDateAppel.Text);


				foreach (AffichageAppel appel in appelsDisplay)
				{
                    //MessageBox.Show("" + appel.societe);
                    if (appel.priorite.Contains(filterPriorite))
					{
						if (appel.traite.Equals(filterTraitement) || filterTraitement == null)
						{
							if (appel.dateEnregistrement >= filterDateDebut || filterDateDebut == null)
							{
								if (appel.societe.ToUpper().Contains(TXT_NomClient.Text.ToUpper()))
								{
                                    if (appel.dateEnregistrement <= filterDateFin || filterDateFin == null)
                                    {
                                        if (appel.destinataire == null || CBB_FullNameDestinataire.Text == "" || appel.destinataire.Contains(CBB_FullNameDestinataire.Text))
                                        {
                                            listAppelFiltered.Add(appel);
                                        }
                                    }
								}
							}
						}						
					}
				}

                appelsDisplay.Clear();
			}
		}
				

		private void BTN_Refresh_Click(object sender, RoutedEventArgs e)
		{			
			ToUpdate();
		}		

		public void BTN_AddAppel_Click(object sender, RoutedEventArgs e)
		{
			AbstractManageAppel form = new AbstractManageAppel(this, oAppelSel, false, false);
			form.Show();
		}

		private void BTN_ModifyAppel_Click(object sender, RoutedEventArgs e)
		{
			ModifyAppel();
		}

		private void ModifyAppel()
		{
            if(DG_Appel.SelectedItem != null) {
                
                AbstractManageAppel form = new AbstractManageAppel(this, oAppelSel, true, false);
                form.Show();
            }
            else
            {
                MessageBox.Show("Merci de sélectionner une ligne du tableau à modifier");
            }
			
		}

		private void BTN_RemoveAppel_Click(object sender, RoutedEventArgs e)
		{

            AffichageAppel appels = (AffichageAppel)DG_Appel.SelectedItem;

            if (appels != null)
			{
				try
				{
                    AffichageAppel appelSelect = (AffichageAppel)DG_Appel.SelectedItem;
                    MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer l'appel de la sociétée " + appelSelect.societe + " ?", "Alerte supression", MessageBoxButton.YesNo);
					if (dr == MessageBoxResult.Yes)
					{
                        var connectionString = Function.connectDatabase();
                        SqlConnection con = new SqlConnection(connectionString);
                        try
                        {
                            con.Open();


                            SqlCommand reqDeeleteMembreAppel = new SqlCommand();
                            reqDeeleteMembreAppel.Connection = con;
                            reqDeeleteMembreAppel.CommandText = "DELETE FROM T_AppelMembres WHERE idAppel = @id";
                            reqDeeleteMembreAppel.Parameters.AddWithValue("@id", appelSelect.idAppel);



                            reqDeeleteMembreAppel.ExecuteNonQuery();

                            SqlCommand reqUpdateHeure = new SqlCommand();
                            reqUpdateHeure.Connection = con;
                            reqUpdateHeure.CommandText = "DELETE FROM Appel WHERE idAppel = @id";
                            reqUpdateHeure.Parameters.AddWithValue("@id", appelSelect.idAppel);



                            reqUpdateHeure.ExecuteNonQuery();

                        }
                        catch
                        {

                        }
                        ToUpdate();
					}
				}
				catch (Exception ex)
				{
                    Function.AfficheException(ex, "Une erreur est survenue lors de la suppression d'un appel : ", "Appel", Log);
                }
			}
		}

		private void BTN_Rechercher_Click(object sender, RoutedEventArgs e)
		{
            listAppelFiltered.Clear();
			ToUpdate();
		}

		private void CBX_CallDateAvailable_Checked(object sender, RoutedEventArgs e)
		{
			DPK_DebutDateAppel.IsEnabled = true;
			DPK_DebutDateAppel.Text = DateTime.Now.AddMonths(-1).ToString();

			DPK_FinDateAppel.IsEnabled = true;
			DPK_FinDateAppel.Text = DateTime.Now.AddMonths(1).ToString();
		}

		private void CBX_CallDateAvailable_Unchecked(object sender, RoutedEventArgs e)
		{
			DPK_DebutDateAppel.IsEnabled = false;
			DPK_FinDateAppel.IsEnabled = false;
			DPK_DebutDateAppel.Text = "";
			DPK_FinDateAppel.Text = "";
			filterDateDebut = DateTime.Now.AddYears(-10);
			filterDateFin = DateTime.Now.AddYears(10);
		}		

		private void BTN_ClearFiltres_Click(object sender, RoutedEventArgs e)
		{
			CBB_FullNameDestinataire.Text = "";
			TXT_NomClient.Text = "";
			CBX_CallDateAvailable.IsChecked = false;
			RBTN_NonTraite.IsChecked = true;
			RBTN_PrioNone.IsChecked = true;
            CBB_FullNameDestinataire.SelectedIndex = -1;
            listAppelFiltered.Clear();
        }
        ListSortDirection sortDirection;

        public void ToUpdate()
		{
            
            toUpdateDG();
            DG_Appel.Items.Refresh();
			Filters();
            
            DG_Appel.ItemsSource = listAppelFiltered;
            applySortDescriptions(sortDirection);
            DG_Appel.Items.Refresh();
        }

		private void TXT_NomClient_KeyUp(object sender, KeyEventArgs e)
		{
			if (CMB_NomClient.Text != TXT_NomClient.Text || TXT_NomClient.Text == "")
			{
				CMB_NomClient.Items.Clear();
			}

			CMB_NomClient.IsDropDownOpen = true;
			dbEntity.Client.Load();
			var clients = dbEntity.Client.Local;

			foreach (Client client in clients)
			{
				if (client.Nom.ToLower().StartsWith(TXT_NomClient.Text.ToLower()))
				{
					CMB_NomClient.Items.Add(client.Nom);
				}
			}

			if (CMB_NomClient.Items.Count == 0)
			{
				CMB_NomClient.IsDropDownOpen = false;
			}

			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}

		public class AbstractManageAppel : ManageAppel
		{
			readonly private DisplayAppel parentAppel;	

			public AbstractManageAppel(DisplayAppel parent, Appel appel, bool isModif, bool isCopy) : base(appel, isModif, isCopy)
			{
				InitializeComponent();
				parentAppel = parent;
			}

            

			internal override void ToUpdateParent()
			{
				parentAppel.dbEntity.SaveChanges();
				parentAppel.ToUpdate();
			}
		}

		private void BTN_ClearDestinataire_Click(object sender, RoutedEventArgs e)
		{
			CBB_FullNameDestinataire.SelectedIndex = -1;
		}

		private void DG_AppelRow_DoubleClick(object sender, MouseButtonEventArgs e)
		{
			ModifyAppel();
		}
		
		private void CheckEnterPressed(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				ToUpdate();
			}
		}

        int selectedColumnIndex;

        private void DG_Appel_Sorting(object sender, DataGridSortingEventArgs e)
        {
            selectedColumnIndex = e.Column.DisplayIndex;
            sortDirection = (e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);

        }

        private void applySortDescriptions(ListSortDirection listSortDirection)
        {
            //Clear current sort descriptions 
            DG_Appel.Items.SortDescriptions.Clear();

            //Get property name to apply sort based on desired column 
            string propertyName = DG_Appel.Columns[selectedColumnIndex].SortMemberPath;

            //Add the new sort description 
            DG_Appel.Items.SortDescriptions.Add(new SortDescription(propertyName, listSortDirection));

            //apply sort 
            applySortDirection(listSortDirection);

            //refresh items to display sort 
            DG_Appel.Items.Refresh();
        }

        private void applySortDirection(ListSortDirection listSortDirection)
        {
            DG_Appel.Columns[selectedColumnIndex].SortDirection = listSortDirection;
        }

       

        private void BTN_CopyAppel_Click(object sender, RoutedEventArgs e)
        {
            if (DG_Appel.SelectedItem != null)
            {
                
                AbstractManageAppel form = new AbstractManageAppel(this, oAppelSel, false, true);
                form.Show();
            }
            else
            {
                MessageBox.Show("Merci de sélectionner une ligne du tableau à modifier");
            }
        }

        public void toUpdateDG()
        {

            listAppelFiltered.Clear();
            DG_Appel.Items.Refresh();

            var reqSociete = from appel in dbEntity.Appel
                             select appel.NomClient;

            var reqTel = from appel in dbEntity.Appel
                             select appel.Tel;

            var reqProjet = from appel in dbEntity.Appel
                             select appel.Projet;

            var reqPriorite = from appel in dbEntity.Appel
                             select appel.Priorite;

            var reqDateSave = from appel in dbEntity.Appel
                             select appel.DateCreation;

            var reqDateTraite = from appel in dbEntity.Appel
                             select appel.DateTraitement;

            var reqTraite = from appel in dbEntity.Appel
                             select appel.Traitement;

            var reqId = from appel in dbEntity.Appel
                                    select appel.idAppel;

            var reqIdColor = from appel in dbEntity.Appel
                             select appel.IdCouleur;

            var reqComment = from appel in dbEntity.Appel
                             select appel.Commentaire;

            var reqContact = from appel in dbEntity.Appel
                             select appel.NomContact;

            var reqTraiteDetail = from appel in dbEntity.Appel
                                  select appel.detailsTraite;

            List<string> listSociete = new List<string>();
            List<string> listTelephone = new List<string>();
            List<string> listProjet = new List<string>();
            List<string> listPriorite = new List<string>();
            List<DateTime> listDateSave = new List<DateTime>();
            List<DateTime> listDateTraite = new List<DateTime>();
            List<Boolean> listTraite= new List<Boolean>();
            List<int> ColorAppel = new List<int>();
            List<int> idAppel = new List<int>();
            List<string> listComment = new List<string>();
            List<string> listContact = new List<string>();
            List<string> listDeatilsTraites = new List<string>();

            foreach(var result in reqIdColor)
            {
                ColorAppel.Add((int)result);
            }

            foreach(var result in reqTraiteDetail)
            {
                listDeatilsTraites.Add(result);
            }

            foreach(var result in reqSociete)
            {
                listSociete.Add(result);
                //MessageBox.Show(result);
            }

            foreach(var result in reqTel)
            {
                listTelephone.Add(result);

            }

            foreach(var result in reqProjet)
            {
                listProjet.Add(result);
            }

            foreach(var result in reqPriorite)
            {
                listPriorite.Add(result);

            }

            foreach(var result in reqDateSave)
            {
                listDateSave.Add((DateTime) result);
            }

            DateTime? date = DateTime.MinValue;
            Nullable<DateTime> dateNull = null;

            foreach (var result in reqDateTraite)
            {
                if(result != null)
                {
                    listDateTraite.Add((DateTime)result);
                }
                else
                {
                    listDateTraite.Add((DateTime)date);
                }
                
                
                
            }

            foreach(var result in reqTraite)
            {
                listTraite.Add((bool)result);
            }

            foreach (var result in reqComment)
            {
                listComment.Add(result);
            }

            foreach (var result in reqContact)
            {
                listContact.Add(result);
            }



            int compteur = 0;
            foreach (var id in reqId)
            {

                idAppel.Add(id);

                var reqDesti = from appelMembre in dbEntity.T_AppelMembres
                               join membres in dbEntity.Membres on appelMembre.idMembres equals membres.id
                               where appelMembre.idAppel == id
                               select membres.FullName;

                StringBuilder desti = new StringBuilder("");

                foreach (var name in reqDesti)
                {
                  
                    desti.Append(name + "  \n");
                }

                int longueur = desti.ToString().Length-2;
                if(longueur > 1)
                {
                    desti.Remove(longueur, 2);
                }

                DateTime UpdatedTime = (DateTime)date;

                int resultDateCompare = DateTime.Compare(listDateTraite[compteur], UpdatedTime);
                if (resultDateCompare == 0)
                {
                    appelsDisplay.Add(new AffichageAppel
                    {
                        idAppel = idAppel[compteur],
                        societe = listSociete[compteur],
                        projet = listProjet[compteur],
                        telephone = listTelephone[compteur],
                        priorite = listPriorite[compteur],
                        traite = listTraite[compteur],
                        dateEnregistrement = listDateSave[compteur],
                        dateTraite = dateNull,
                        destinataire = desti.ToString(),
                        colorAppel = ColorAppel[compteur],
                        comment = listComment[compteur],
                        contact = listContact[compteur],
                        detailsTraite = listDeatilsTraites[compteur]

                    });
                }
                else
                {
                    appelsDisplay.Add(new AffichageAppel
                    {
                        idAppel= idAppel[compteur],
                        societe = listSociete[compteur],
                        projet = listProjet[compteur],
                        telephone = listTelephone[compteur],
                        priorite = listPriorite[compteur],
                        traite = listTraite[compteur],
                        dateEnregistrement = listDateSave[compteur],
                        dateTraite = listDateTraite[compteur],
                        destinataire = desti.ToString(),
                        colorAppel = ColorAppel[compteur],
                        comment = listComment[compteur],
                        contact = listContact[compteur],
                        detailsTraite = listDeatilsTraites[compteur]

                    });
                }

                
                
                

                compteur = compteur +1;
            }

            
        }

        public class AffichageAppel
        {
            public int idAppel { get; set; }
            public string societe { get; set; }

            public string telephone { get; set; }

            public string projet { get; set; }

            public string priorite { get; set; }

            public DateTime dateEnregistrement { get; set; }

            public DateTime? dateTraite { get; set; }

            public Boolean traite { get; set; }

            public string destinataire { get; set; }

            public int colorAppel { get; set; }

            public string comment { get; set; }

            public string contact { get; set; }

            public string detailsTraite { get; set; }
        }

        private void DG_Appel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AffichageAppel appels = (AffichageAppel)DG_Appel.SelectedItem;

            try
            {
                oAppelSel = new Appel()
                {
                    NomClient = appels.societe,
                    Projet = appels.projet,
                    DateCreation = appels.dateEnregistrement,
                    DateTraitement = appels.dateTraite,
                    Traitement = appels.traite,
                    IdCouleur = appels.colorAppel,
                    Priorite = appels.priorite,
                    Tel = appels.telephone,
                    Commentaire = appels.comment,
                    NomContact = appels.contact,
                    idAppel = appels.idAppel,
                    detailsTraite = appels.detailsTraite,
                    
                   
                };
            }
            catch
            {
            }
        }
    }
}
