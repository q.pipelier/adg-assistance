﻿using ADG_Assistance.DB;
using ADG_Assistance.Functions;
using ADG_Assistance.Vue.DataManagement;
using NLog;
using System;
using System.Linq;
using System.Runtime.Caching;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
namespace ADG_Assistance.Vue.DataDisplay
{
	/// <summary>
	/// Logique d'interaction pour DisplayPersonnel.xaml
	/// </summary>
	public partial class DisplayPersonnel : Page
	{
        readonly Logger Log = LogManager.GetCurrentClassLogger();
        ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();
        ObjectCache cache = MemoryCache.Default;
        string role;
        public Boolean IsModify
        {
            get; set;
        }
		Membres oMembresSel;
		MainWindow m_oWin;


		public DisplayPersonnel(MainWindow oWin)
		{
            IsModify = false;
            m_oWin = oWin;
			InitializeComponent();
            role = cache.Get("Role").ToString();
            if(role != "Administrateur")
            {
                BTN_AddPersonnel.Visibility = Visibility.Hidden;
                BTN_ModifyPersonnel.Visibility = Visibility.Hidden;
                BTN_DeletePersonnel.Visibility = Visibility.Hidden;
            }
		}

		#region eventMethods
		private void BTN_ModifyPersonnel_Click(object sender, RoutedEventArgs e)
		{
			ModifyPersonnel();
		}

		private void ModifyPersonnel()
		{
			IsModify = true;
			oMembresSel = (Membres)DG_Personnel.SelectedItem;
			ManagePersonnel managePersonnel = new ManagePersonnel(this, oMembresSel);
			managePersonnel.Show();
		}

		private void BTN_AddPersonnel_Click(object sender, RoutedEventArgs e)
		{
			IsModify = false;
			ManagePersonnel managePersonnel = new ManagePersonnel(this);
			managePersonnel.Show();
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			m_oWin.Title = Title;
			ToUpdate();
		}

		private void BTN_DeletePersonnel_Click(object sender, RoutedEventArgs e)
		{
			oMembresSel = (Membres)DG_Personnel.SelectedItem;
			deletePersonnel(oMembresSel);
		}
		#endregion

		/// <summary>
		/// Vérifie qu'un item est sélectionné puis supprime une personne
		/// </summary>
		private void deletePersonnel(Membres selectedPersonnel)
		{
			if (selectedPersonnel != null)
			{
				try
				{
					MessageBoxResult dr = MessageBox.Show("Etes vous sur de vouloir supprimer " + selectedPersonnel.prenom + " " + selectedPersonnel.nom, "Alerte supression", MessageBoxButton.YesNo);

					if (dr == MessageBoxResult.Yes)
					{
						dbEntity.Membres.Remove(selectedPersonnel);
						dbEntity.SaveChanges();
					}
				}
				catch (Exception ex)
				{
                    Function.AfficheException(ex, "Une erreur est survenue lors de la suppression d'un membre du personnel : ", "Personnel", Log);
                }
			}
			ToUpdate();
		}

		public void ToUpdate()
		{
			dbEntity.SaveChanges();

			var qMembres = from membres in dbEntity.Membres
							 select membres;

			DG_Personnel.ItemsSource = qMembres.ToList();
		}

		private void DG_PersonnelRow_DoubleClick(object sender, MouseButtonEventArgs e)
		{
            if (role == "Administrateur")
            {

                ModifyPersonnel();
            }
		}


	}
}