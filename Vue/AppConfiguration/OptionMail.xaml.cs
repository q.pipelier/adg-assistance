﻿using ADG_Assistance.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue.AppConfiguration
{
	/// <summary>
	/// Logique d'interaction pour OptionMail.xaml
	/// </summary>
	public partial class OptionMail : Page
	{
		private Mail dataMail;
		private Parametre dataConfiguration;
		ADG_AssistanceEntities dbEntity = new ADG_AssistanceEntities();

		public OptionMail()
		{
			InitializeComponent();

			dataMail = dbEntity.Mail.First();
			dataConfiguration = dbEntity.Parametre.First();

			fillFields();

			DG_InfoCodeMail.ItemsSource = (from infoCodeMail in dbEntity.InfoCodeMail select infoCodeMail).ToList();
		}

		private void BTN_Accept_Click(object sender, RoutedEventArgs e)
		{
			if (TXT_BodyMail.Text == "" || TXT_HeaderMail.Text == "" || TXT_EmailAdg.Text == "" || TXT_Password.Text == "" || TXT_Port.Text == "" || TXT_SMTP.Text == "")
			{
				MessageBox.Show("Merci de compléter tout les champs");
			}
			else
			{
				dataMail.Body = TXT_BodyMail.Text;
				dataMail.Subject = TXT_HeaderMail.Text;
				dataConfiguration.email = TXT_EmailAdg.Text;
				dataConfiguration.password = TXT_Password.Text;
				dataConfiguration.port = int.Parse(TXT_Port.Text);
				dataConfiguration.smtp = TXT_SMTP.Text;

				dbEntity.SaveChanges();
			}
		}

		private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
		{
			fillFields();
		}

		private void fillFields()
		{
			TXT_BodyMail.Text = dataMail.Body.ToString();
			TXT_HeaderMail.Text = dataMail.Subject.ToString();
			TXT_EmailAdg.Text = dataConfiguration.email.ToString();
			TXT_Password.Text = dataConfiguration.password.ToString();
			TXT_Port.Text = dataConfiguration.port.ToString();
			TXT_SMTP.Text = dataConfiguration.smtp.ToString();
		}
	}
}
