﻿using ADG_Assistance.Vue.AppConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ADG_Assistance.Vue
{
	/// <summary>
	/// Logique d'interaction pour Configuration.xaml
	/// </summary>
	public partial class Configuration : Page
	{
		MainWindow m_oWin;

		public Configuration(MainWindow oWin)
		{
			InitializeComponent();
			this._NavigationConfiguration.Navigate(new OptionAppel());
			m_oWin = oWin;
		}

		private void BTN_ConfigurationAppel_Click(object sender, RoutedEventArgs e)
		{
			this._NavigationConfiguration.Navigate(new OptionAppel());
		}

		private void BTN_ConfigurationMail_Click(object sender, RoutedEventArgs e)
		{
			this._NavigationConfiguration.Navigate(new OptionMail());
		}

		private void BTN_ResourcesOption_Click(object sender, RoutedEventArgs e)
		{
			this._NavigationConfiguration.Navigate(new OptionResources());
		}

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			m_oWin.Title = this.Title;
		}
	}
}
