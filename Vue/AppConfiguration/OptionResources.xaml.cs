﻿using ADG_Assistance.DB;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace ADG_Assistance.Vue.AppConfiguration
{
	/// <summary>
	/// Logique d'interaction pour OptionResources.xaml
	/// </summary>
	public partial class OptionResources : Page
	{

		public OptionResources()
		{
			InitializeComponent();
		}

		private void BTN_WordBIBrowse_Click(object sender, RoutedEventArgs e)
		{
            throw new NotSupportedException();
		}

		private void BTN_WordModelCM_Click(object sender, RoutedEventArgs e)
		{
            throw new NotSupportedException();
        }

		private void BTN_WordDossierBI_Click(object sender, RoutedEventArgs e)
		{
            throw new NotSupportedException();
        }

		private void BTN_WordDossierCM_Click(object sender, RoutedEventArgs e)
		{
            throw new NotSupportedException();
        }
	}
}
