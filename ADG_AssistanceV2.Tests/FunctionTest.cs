﻿using System;
using ADG_AssistanceV2.Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ADG_AssistanceV2.Tests
{
    [TestClass]
    public class FunctionTest
    {
        [TestMethod]
        public void stringToDate_Error()
        {
           Assert.AreEqual( null, Function.stringToDate("xx/xxx/xxxx"));
        }

        [TestMethod]
        public void stringToDate_Overflow()
        {
            Assert.AreEqual(null, Function.stringToDate("30/02/2018"));
        }

        [TestMethod]
        public void stringToDate_OK()
        {
            DateTime dtDate = new DateTime(2018, 2, 1);
            Assert.AreEqual(dtDate, Function.stringToDate("01/02/2018"));
        }

        [TestMethod]
        public void stringToDate_Format_error()
        {
            Assert.AreEqual(null, Function.stringToDate("01*02*2018"));
        }
    }
}
