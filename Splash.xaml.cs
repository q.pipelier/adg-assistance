﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Deployment.Application;
using System.Windows.Threading;

namespace ADG_Assistance
{
	/// <summary>
	/// Logique d'interaction pour Splash.xaml
	/// </summary>
	public partial class Splash : Window
	{
		public Splash()
		{
			InitializeComponent();
		}

		private void dtClockTime_Tick(object sender, EventArgs e)
		{
			Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			LBL_AppTitle.Content = "ADG_Assistance";

			string version;
			try
			{
				version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
			}
			catch (Exception)
			{
				version = "- Pas de versions en Debug";
			}
			

			LBL_Version.Content = LBL_Version.Content + " " + version;

			if (DateTime.Now.Year == 2017)
			{
				LBL_Copiright.Content = "Copyright © ADG Software Engineering 2017";
			}
			else
			{
				LBL_Copiright.Content = "Copyright © ADG Software Engineering 2017 - " + DateTime.Now.Year;
			}

			DispatcherTimer dtClockTime = new DispatcherTimer();

			dtClockTime.Interval = new TimeSpan(0, 0, 5); //in Hour, Minutes, Second.
			dtClockTime.Tick += dtClockTime_Tick;

			dtClockTime.Start();
		}

		private void Window_LostFocus(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Close();
		}
	}
}
