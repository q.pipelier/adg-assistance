﻿
using ADG_Assistance.Functions;
using ADG_Assistance.Vue;
using ADG_Assistance.Vue.DataDisplay;
using ADG_Assistance.Vue.DataManagement;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace ADG_Assistance
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObjectCache cache = MemoryCache.Default;
        DispayLogin login = new DispayLogin();
        string role;
        public MainWindow()
        {
            CheckDataBase();
            this.Hide();
            Splash splash = new Splash();
            splash.ShowDialog();

            login.Show();

            


        }
        public MainWindow(bool sender)
        {
            if (sender)
            {
                login.Close();
                object roleConvert = cache.Get("Role");
                role = roleConvert.ToString();
                this.Show();
                InitializeComponent();
                UpdateForegroundColor();
                _NavigationFrame.Navigate(new DisplayAppel(this));
                DisplayAppel.Foreground = new SolidColorBrush(Colors.Pink);

                if(role != "Administrateur"){
                    
                    this.ShowConfiguration.Visibility = Visibility.Hidden;
                }

                if (!Directory.Exists(@"C:\Document_ADG_Assistance\BI"))
                {
                    Directory.CreateDirectory(@"C:\Document_ADG_Assistance\BI");
                }

                if (role == "Administrateur")
                {
                    if (!Directory.Exists(@"C:\Document_ADG_Assistance\CM"))
                    {
                        Directory.CreateDirectory(@"C:\Document_ADG_Assistance\CM");
                    }
                }
            }
        }

        private void DisplayAppel_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayAppel(this));
            DisplayAppel.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void ShowConfiguration_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new Configuration(this));
            ShowConfiguration.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayPersonnel_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayPersonnel(this));
            DisplayPersonnel.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayProjet_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayProjet(this));
            DisplayProjet.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayClient_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayClient(this));
            DisplayClient.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayContact_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayContact(this));
            DisplayContact.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayContrat_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayContrat(this));
            DisplayContrat.Foreground = new SolidColorBrush(Colors.Pink);
        }

        private void DisplayBI_Click(object sender, RoutedEventArgs e)
        {
            UpdateForegroundColor();
            this._NavigationFrame.Navigate(new DisplayBI(this));
            DisplayBI.Foreground = new SolidColorBrush(Colors.Pink);
        }



        private void UpdateForegroundColor()
        {
            DisplayAppel.Foreground = new SolidColorBrush(Colors.White);
            ShowConfiguration.Foreground = new SolidColorBrush(Colors.White);
            DisplayPersonnel.Foreground = new SolidColorBrush(Colors.White);
            DisplayProjet.Foreground = new SolidColorBrush(Colors.White);
            DisplayClient.Foreground = new SolidColorBrush(Colors.White);
            DisplayContact.Foreground = new SolidColorBrush(Colors.White);
            DisplayContrat.Foreground = new SolidColorBrush(Colors.White);
            DisplayBI.Foreground = new SolidColorBrush(Colors.White);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void CheckDataBase()
        {
            
            var connectionString = Function.connectDatabase();
            SqlConnection con = new SqlConnection(connectionString);
            try
            {
                con.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("La connexion à la base de donnée a échouée");
                System.Environment.Exit(1);
            }
        }


    }
}
